<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_paket extends CI_Controller {

	var $data = array();

	function __construct() {
		parent::__construct();
		
		// profiler
		// $this->output->enable_profiler($this->config->item('profiler_status'));

		/*
		if(!$this->session->barangdata('ptbr_admin')) {
			redirect('auth');
		}
		else {
			$adminData = $this->session->barangdata('ptbr_admin');
			$this->data['admin_name'] = $adminData['name'];
			$this->data['admin_photo'] = $adminData['photo'];
			$this->data['admin_lastLogin'] = $adminData['lastLogin'];
			$this->data['admin_barangName'] = $adminData['barangName'];

			$this->load->model('bon_sopir_model', 'bon_sopir_model');
			$this->load->model('pegawai_model', 'pegawai_model');
		}
		*/

		## load model here 
		$this->load->model('paket_model', 'm_paket');
		$this->load->model('kategori_model', 'm_kategori');
		$this->load->model('user_model', 'm_user');
		$this->load->model('barang_model', 'm_barang');
	}

	public function index()	{
		$data = $this->data;

		$data['list_user'] = $this->m_user->getAllData();

		//$data['list'] = $this->m_paket->getAllData();
		$data['list'] = $this->m_paket->getAllDataperUser($this->session->userdata['logged_in']['id']);	

		$data['barang'] = $this->m_barang->getAllDataperUser($this->session->userdata['logged_in']['id']);		

		$this->load->view('paket_data', $data);
	}

	public function submitAddData() {
		$this->m_paket->inputData();

		$this->session->set_flashdata('input_success', 'Input data bon berhasil.');

		redirect('c_paket');
	}

	public function edit($id) {
		$data = $this->data;

		if (!empty($id)) {
			$data['list_edit'] = $this->m_paket->getAllDataByID($id) ;
		} 
		
		$data['barang'] = array();
		
		$id_barang = explode(',', $data['list_edit'][0]->id_barang) ;

		foreach ($id_barang as $key => $value) {
			$data['barang'][]= $this->m_barang->getAllDataByID($value) ;
		}

	    $this->output->set_content_type('application/json');
	    
	    $this->output->set_output(json_encode($data));

	    return $data;
	}

	public function submitEditData() {
		$this->m_paket->editData($this->input->post('id'));

		if (!empty($_FILES['photo_paket'])) {
			$config['upload_path']          = './upload/paket/';
			$config['allowed_types']        = 'jpg|gif|png|jpeg|JPG|PNG';

			$this->load->library('upload', $config);

            if ( ! $this->upload->do_upload('photo_paket'))
            {
                    $error = array('error' => $this->upload->display_errors());
            }
            else
            {
                    $data = array('upload_data' => $this->upload->data());
            }
		}

		if (!empty($_FILES['photo_paket1'])) {
			$config['upload_path']          = './upload/paket/';
			$config['allowed_types']        = 'jpg|gif|png|jpeg|JPG|PNG';

			$this->load->library('upload', $config);

            if ( ! $this->upload->do_upload('photo_paket1'))
            {
                    $error = array('error' => $this->upload->display_errors());
            }
            else
            {
                    $data = array('upload_data' => $this->upload->data());
            }
		}

		if (!empty($_FILES['photo_paket2'])) {
			$config['upload_path']          = './upload/paket/';
			$config['allowed_types']        = 'jpg|gif|png|jpeg|JPG|PNG';

			$this->load->library('upload', $config);

            if ( ! $this->upload->do_upload('photo_paket2'))
            {
                    $error = array('error' => $this->upload->display_errors());
            }
            else
            {
                    $data = array('upload_data' => $this->upload->data());
            }
		}

		if (!empty($_FILES['photo_paket3'])) {
			$config['upload_path']          = './upload/paket/';
			$config['allowed_types']        = 'jpg|gif|png|jpeg|JPG|PNG';

			$this->load->library('upload', $config);

            if ( ! $this->upload->do_upload('photo_paket3'))
            {
                    $error = array('error' => $this->upload->display_errors());
            }
            else
            {
                    $data = array('upload_data' => $this->upload->data());
            }
		}

		if (!empty($_FILES['photo_paket4'])) {
			$config['upload_path']          = './upload/paket/';
			$config['allowed_types']        = 'jpg|gif|png|jpeg|JPG|PNG';

			$this->load->library('upload', $config);

            if ( ! $this->upload->do_upload('photo_paket4'))
            {
                    $error = array('error' => $this->upload->display_errors());
            }
            else
            {
                    $data = array('upload_data' => $this->upload->data());
            }
		}

		$this->session->set_flashdata('input_success', 'Ubah data berhasil.');

		redirect('c_paket');
	}

	public function delete($id) {
		$reference = $this->m_paket->deleteData($id);

		$this->session->set_flashdata('delete_success', 'Data berhasil dihapus.');

		redirect('c_paket');
	}

	public function barangperuser()	{
		$data = $this->data;

		$data['list_kategori'] = $this->m_kategori->getAllData();
		$data['list_edit'] = $this->m_user->getAllDataByID($this->session->userdata['logged_in']['id']);
		$data['list'] = $this->m_paket->getAllDataperUser($this->session->userdata['logged_in']['id']);

		$this->load->view('paket_data_peruser', $data);
	}

	public function submitAddDataperUser() {
		$this->m_paket->inputData();

		$this->session->set_flashdata('input_success', 'Input data bon berhasil.');

		redirect('c_paket/barangperuser');
	}

	public function submitEditDataperuser() {
		$this->m_paket->editData($this->input->post('id'));


		if (!empty($_FILES)) {
			$config['upload_path']          = './upload/barang/';
			$config['allowed_types']        = 'jpg|gif|png|jpeg|JPG|PNG';

			$this->load->library('upload', $config);

            if ( ! $this->upload->do_upload('photo_barang'))
            {
                    $error = array('error' => $this->upload->display_errors());
            }
            else
            {
                    $data = array('upload_data' => $this->upload->data());
            }
		}

		$this->session->set_flashdata('input_success', 'Ubah data berhasil.');


		$this->session->set_flashdata('input_success', 'Ubah data berhasil.');

		redirect('c_paket/barangperuser');
	}

	public function deleteperuser($id) {
		$reference = $this->m_paket->deleteData($id);

		$this->session->set_flashdata('delete_success', 'Data berhasil dihapus.');

		redirect('c_paket/barangperuser');
	}
}
