<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_user extends CI_Controller {

	var $data = array();

	function __construct() {
		parent::__construct();
		
		// profiler
		// $this->output->enable_profiler($this->config->item('profiler_status'));

		/*
		if(!$this->session->userdata('ptbr_admin')) {
			redirect('auth');
		}
		else {
			$adminData = $this->session->userdata('ptbr_admin');
			$this->data['admin_name'] = $adminData['name'];
			$this->data['admin_photo'] = $adminData['photo'];
			$this->data['admin_lastLogin'] = $adminData['lastLogin'];
			$this->data['admin_userName'] = $adminData['userName'];

			$this->load->model('bon_sopir_model', 'bon_sopir_model');
			$this->load->model('pegawai_model', 'pegawai_model');
		}
		*/
		if (empty($this->session->userdata['logged_in']['id'])) {
			$data['msg'] = 'Anda Harus Login';
			redirect('c_f_home',$data);
		}

		## load model here 
		$this->load->model('user_model', 'm_user');
		$this->load->model('role_model', 'm_role');
		$this->load->model('order_model', 'm_order');
	}

	public function index()	{
		$data = $this->data;

		$data['list_role'] = $this->m_role->getAllData();
		$data['list'] = $this->m_user->getAllData();

		$this->load->view('user_data', $data);
	}

	public function profil(){
		$data = $this->data;
		$data['msg'] = '';
		$data['psanana'] = '';		

		$id = $this->session->userdata['logged_in']['id'];

		$data['list_edit'] = $this->m_user->getAllDataByID($id) ;
		$data['list_admin'] = $this->m_user->getAllDataByAdmin() ;

		
		$data['order'] = $this->m_order->getOrder($this->session->userdata['logged_in']['id']);
		$data['orderpaket'] = $this->m_order->getOrderPaket($this->session->userdata['logged_in']['id']);

		if ($data['order']->pesanan != '0') {

			$data['msg'] = 'Anda Memiliki Pesanan Baru' ;

			if ($data['orderpaket']->pesanan != '0') {
				$data['psanana'] = $data['order']->pesanan .' Jasa dan ' . $data['orderpaket']->pesanan . ' Paket';
			} else {
				$data['psanana'] = $data['order']->pesanan .' Barang';
			}
			
		}

		$this->load->view('profil', $data);
	}

	public function submitAddData() {
		$this->m_user->inputData();

		redirect('c_user');
	}

	public function edit($id) {
		$data = $this->data;

		if (!empty($id)) {
			$data['list_edit'] = $this->m_user->getAllDataByID($id) ;
		} 
		
	    $this->output->set_content_type('application/json');
	    
	    $this->output->set_output(json_encode($data));

	    return $data;
	}

	public function submitEditData() {
		$this->m_user->editData($this->input->post('id'));

		$this->session->set_flashdata('input_success', 'Ubah data berhasil.');

		redirect('c_user');
	}


	public function submitEditDataProfil() {
		$this->m_user->editData($this->input->post('id'));

		
		$this->load->library('upload');
		if (!empty($_FILES['user_photo_langganan'])) {
			
			$config['upload_path']          = './upload/buktilangganan/';
			$config['allowed_types']        = 'jpg|gif|png|jpeg|JPG|PNG';

			$this->load->library('upload', $config);
			$this->upload->initialize($config);

            if ( ! $this->upload->do_upload('user_photo_langganan'))
            {	
            	
                    $error = array('error' => $this->upload->display_errors());
                    print_r($error);die();
            }
            else
            {	
            	
                    $data = array('upload_data' => $this->upload->data());
            }
		}

		
		if (!empty($_FILES['user_photo'])) {
			
			$config['upload_path']          = './upload/userphoto/';
			$config['allowed_types']        = 'jpg|gif|png|jpeg|JPG|PNG';

			$this->upload->initialize($config);	

			$this->load->library('upload', $config);

			if ( ! $this->upload->do_upload('user_photo'))
            {
                    $error = array('error' => $this->upload->display_errors());
            }
            else
            {
                    $data = array('upload_data' => $this->upload->data());
            }
		}
		


		$this->session->set_flashdata('input_success', 'Ubah data berhasil.');

		redirect('c_user/profil');
	}

	public function delete($id) {
		$reference = $this->m_user->deleteData($id);

		$this->session->set_flashdata('delete_success', 'Data berhasil dihapus.');

		redirect('c_user');
	}

	public function logout() {
		if($this->session->userdata('logged_in')) {
			$this->session->unset_userdata('logged_in');
			$this->session->sess_destroy();
		}
		
        redirect('c_f_home');
	}
}
