<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_barang extends CI_Controller {

	var $data = array();

	function __construct() {
		parent::__construct();
		
		// profiler
		// $this->output->enable_profiler($this->config->item('profiler_status'));

		/*
		if(!$this->session->barangdata('ptbr_admin')) {
			redirect('auth');
		}
		else {
			$adminData = $this->session->barangdata('ptbr_admin');
			$this->data['admin_name'] = $adminData['name'];
			$this->data['admin_photo'] = $adminData['photo'];
			$this->data['admin_lastLogin'] = $adminData['lastLogin'];
			$this->data['admin_barangName'] = $adminData['barangName'];

			$this->load->model('bon_sopir_model', 'bon_sopir_model');
			$this->load->model('pegawai_model', 'pegawai_model');
		}
		*/

		## load model here 
		$this->load->model('barang_model', 'm_barang');
		$this->load->model('kategori_model', 'm_kategori');
		$this->load->model('user_model', 'm_user');
	}

	public function index()	{
		$data = $this->data;

		$data['list_kategori'] = $this->m_kategori->getAllData();
		//$data['list_user'] = $this->m_user->getAllData();
		$data['list_user'] = $this->m_user->getAllDataPenjual();
		$data['list'] = $this->m_barang->getAllData();

		$this->load->view('barang_data', $data);
	}

	public function submitAddData() {
		$this->m_barang->inputData();

		$this->session->set_flashdata('input_success', 'Input data bon berhasil.');

		redirect('c_barang');
	}

	public function edit($id) {
		$data = $this->data;

		if (!empty($id)) {
			$data['list_edit'] = $this->m_barang->getAllDataByID($id) ;
		} 
		
	    $this->output->set_content_type('application/json');
	    
	    $this->output->set_output(json_encode($data));

	    return $data;
	}

	public function submitEditData() {
		$this->m_barang->editData($this->input->post('id'));

		$this->session->set_flashdata('input_success', 'Ubah data berhasil.');

		redirect('c_barang');
	}

	public function delete($id) {
		$reference = $this->m_barang->deleteData($id);

		$this->session->set_flashdata('delete_success', 'Data berhasil dihapus.');

		redirect('c_barang');
	}

	public function barangperuser()	{
		$data = $this->data;

		$data['list_kategori'] = $this->m_kategori->getAllData();
		$data['list_edit'] = $this->m_user->getAllDataByID($this->session->userdata['logged_in']['id']);
		$data['list'] = $this->m_barang->getAllDataperUser($this->session->userdata['logged_in']['id']);

		$this->load->view('barang_data_peruser', $data);
	}

	public function submitAddDataperUser() {
		$this->m_barang->inputData();

		$this->session->set_flashdata('input_success', 'Input data bon berhasil.');

		redirect('c_barang/barangperuser');
	}

	public function submitEditDataperuser() {
		$this->m_barang->editData($this->input->post('id'));


		if (!empty($_FILES['photo_barang'])) {
			$config['upload_path']          = './upload/barang/';
			$config['allowed_types']        = 'jpg|gif|png|jpeg|JPG|PNG';

			$this->load->library('upload', $config);

            if ( ! $this->upload->do_upload('photo_barang'))
            {
                    $error = array('error' => $this->upload->display_errors());
            }
            else
            {
                    $data = array('upload_data' => $this->upload->data());
            }
		}


		for ($i=1; $i < 5; $i++) { 
			if (!empty($_FILES['photo_barang'.$i])) {
				$config['upload_path']          = './upload/barang/';
				$config['allowed_types']        = 'jpg|gif|png|jpeg|JPG|PNG';

				$this->load->library('upload', $config);

	            if ( ! $this->upload->do_upload('photo_barang'.$i))
	            {
	                    $error = array('error' => $this->upload->display_errors());
	            }
	            else
	            {
	                    $data = array('upload_data' => $this->upload->data());
	            }
			}

		}
		$this->session->set_flashdata('input_success', 'Ubah data berhasil.');


		

		redirect('c_barang/barangperuser');
	}

	public function deleteperuser($id) {
		$reference = $this->m_barang->deleteData($id);

		$this->session->set_flashdata('delete_success', 'Data berhasil dihapus.');

		redirect('c_barang/barangperuser');
	}
}
