<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_order extends CI_Controller {

	var $data = array();

	function __construct() {
		parent::__construct();
		
		// profiler
		// $this->output->enable_profiler($this->config->item('profiler_status'));

		/*
		if(!$this->session->barangdata('ptbr_admin')) {
			redirect('auth');
		}
		else {
			$adminData = $this->session->barangdata('ptbr_admin');
			$this->data['admin_name'] = $adminData['name'];
			$this->data['admin_photo'] = $adminData['photo'];
			$this->data['admin_lastLogin'] = $adminData['lastLogin'];
			$this->data['admin_barangName'] = $adminData['barangName'];

			$this->load->model('bon_sopir_model', 'bon_sopir_model');
			$this->load->model('pegawai_model', 'pegawai_model');
		}
		*/

		## load model here 
		$this->load->model('barang_model', 'm_barang');
		$this->load->model('kategori_model', 'm_kategori');
		$this->load->model('user_model', 'm_user');
		$this->load->model('order_model', 'm_order');
		$this->load->model('orderpaket_model', 'm_orderpaket');
	}

	public function index()	{
		$data = $this->data;

		redirect('C_order/pesananperuser');
	}

	public function edit($id) {
		$data = $this->data;

		if (!empty($id)) {
			$data['list_edit'] = $this->m_order->getAllDataByID($id) ;
		} 
		
	    $this->output->set_content_type('application/json');
	    
	    $this->output->set_output(json_encode($data));

	    return $data;
	}

	public function editpaket($id) {
		$data = $this->data;

		if (!empty($id)) {
			$data['list_edit'] = $this->m_orderpaket->getAllDataByID($id) ;
		} 
		
	    $this->output->set_content_type('application/json');
	    
	    $this->output->set_output(json_encode($data));

	    return $data;
	}


	public function pesananperuser() {
		$data = $this->data;

		$data['list'] = $this->m_order->getAllDataperUser($this->session->userdata['logged_in']['id']);

		$this->load->view('order_data_peruser', $data);
	}

	public function pesananperuserpaket() {
		$data = $this->data;
		
		$data['list'] = $this->m_orderpaket->getAllDataperUser($this->session->userdata['logged_in']['id']);

		$this->load->view('order_data_peruser_paket', $data);
	}


	public function pesananpembeli() {
		$data = $this->data;
		$data['msg'] = '';
		

		if (!empty($this->session->userdata['logged_in']['id'])) {
			$data['list'] = $this->m_order->getOrderPembeli($this->session->userdata['logged_in']['id']);
		} else {
			$data['msg'] = 'Anda Belum Login';
		}		

		$this->load->view('order_data_pembeli', $data);
	}

	public function pesananpembelipaket() {
		$data = $this->data;

		$data['list'] = $this->m_orderpaket->getOrderPembeli($this->session->userdata['logged_in']['id']);

		$this->load->view('order_data_pembeli_paket', $data);
	}

	public function submitEditDataperuser() {
		$this->m_order->editData($this->input->post('id'));

		if (!empty($_FILES)) {
			$config['upload_path']          = './upload/buktibayar/';
			$config['allowed_types']        = 'jpg|gif|png|jpeg|JPG|PNG';

			$this->load->library('upload', $config);

            if ( ! $this->upload->do_upload('bukti_bayar'))
            {
                    $error = array('error' => $this->upload->display_errors());
            }
            else
            {
                    $data = array('upload_data' => $this->upload->data());
            }
		}

		
		$this->session->set_flashdata('input_success', 'Ubah data berhasil.');

		redirect('c_order/pesananperuser');
	}

	public function submitEditDataperuserpaket() {

		$this->m_orderpaket->editData($this->input->post('id'));

		if (!empty($_FILES)) {
			$config['upload_path']          = './upload/buktibayar/';
			$config['allowed_types']        = 'jpg|gif|png|jpeg|JPG|PNG';

			$this->load->library('upload', $config);

            if ( ! $this->upload->do_upload('bukti_bayar'))
            {
                    $error = array('error' => $this->upload->display_errors());
            }
            else
            {
                    $data = array('upload_data' => $this->upload->data());
            }
		}

		$this->session->set_flashdata('input_success', 'Ubah data berhasil.');


		$this->session->set_flashdata('input_success', 'Ubah data berhasil.');

		redirect('c_order/pesananperuser');
	}

	public function submitEditDatapembeli() {

		$this->m_order->editData($this->input->post('id'));

		if (!empty($_FILES)) {
			$config['upload_path']          = './upload/buktibayar/';
			$config['allowed_types']        = 'jpg|gif|png|jpeg|JPG|PNG';

			$this->load->library('upload', $config);

            if ( ! $this->upload->do_upload('bukti_bayar'))
            {
                    $error = array('error' => $this->upload->display_errors());
            }
            else
            {
                    $data = array('upload_data' => $this->upload->data());
            }
		}

		$this->session->set_flashdata('input_success', 'Ubah data berhasil.');


		$this->session->set_flashdata('input_success', 'Ubah data berhasil.');

		redirect('c_order/pesananpembeli');
	}

	public function submitEditDatapembelipaket() {

		$this->m_orderpaket->editData($this->input->post('id'));

		if (!empty($_FILES)) {
			$config['upload_path']          = './upload/buktibayarpaket/';
			$config['allowed_types']        = 'jpg|gif|png|jpeg|JPG|PNG';

			$this->load->library('upload', $config);

            if ( ! $this->upload->do_upload('bukti_bayar_paket'))
            {
                    $error = array('error' => $this->upload->display_errors());
            }
            else
            {
                    $data = array('upload_data' => $this->upload->data());
            }
		}

		$this->session->set_flashdata('input_success', 'Ubah data berhasil.');


		$this->session->set_flashdata('input_success', 'Ubah data berhasil.');

		redirect('c_order/pesananpembeli');
	}


	public function deleteperuser($id) {
		$reference = $this->m_order->deleteData($id);

		$this->session->set_flashdata('delete_success', 'Data berhasil dihapus.');

		redirect('c_order');
	}

	public function deleteperuserpaket($id) {
		$reference = $this->m_orderpaket->deleteData($id);

		$this->session->set_flashdata('delete_success', 'Data berhasil dihapus.');

		redirect('c_order/pesananperuserpaket');
	}
}
