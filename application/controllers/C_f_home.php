<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_f_home extends CI_Controller {

	var $data = array();

	function __construct() {
		parent::__construct();
		
		// profiler
		// $this->output->enable_profiler($this->config->item('profiler_status'));

		/*
		if(!$this->session->barangdata('ptbr_admin')) {
			redirect('auth');
		}
		else {
			$adminData = $this->session->barangdata('ptbr_admin');
			$this->data['admin_name'] = $adminData['name'];
			$this->data['admin_photo'] = $adminData['photo'];
			$this->data['admin_lastLogin'] = $adminData['lastLogin'];
			$this->data['admin_barangName'] = $adminData['barangName'];

			$this->load->model('bon_sopir_model', 'bon_sopir_model');
			$this->load->model('pegawai_model', 'pegawai_model');
		}
		*/

		## load model here 
		$this->load->model('user_model', 'm_user');
		$this->load->model('paket_model', 'm_paket');
		$this->load->model('barang_model', 'm_barang');
		$this->load->model('order_model', 'm_order');
		$this->load->model('orderpaket_model', 'm_orderpaket');
		$this->load->model('kategori_model', 'm_kategori');
		
	}

	public function index()	{
		$data['msg'] = '';
		if (!empty($_POST['cari'])) {
			$data['list'] = $this->m_barang->getAllDatabysearch($_POST['cari']);
		} else {
			$data['list'] = $this->m_barang->getAllData();	
		}
		
		$data['kategori'] = $this->m_kategori->getAllData();

		$this->load->view('front/index_front',$data);
	}

	public function admin()	{
		$data['msg'] = '';
		if (!empty($_POST['cari'])) {
			$data['list'] = $this->m_barang->getAllDatabysearch($_POST['cari']);
		} else {
			$data['list'] = $this->m_barang->getAllData();	
		}
		
		$data['kategori'] = $this->m_kategori->getAllData();

		$this->load->view('front/index_front_admin',$data);
	}

	public function register() {
		$data['kategori'] = $this->m_kategori->getAllData();

		$result = $this->m_user->inputData();

		if (empty($result)) {
			$data['msg'] = 'Pendaftaran Berhasil';
		}  else {			
			$data['msg'] = 'Pendaftaran Gagal';
		}

		$this->load->view('front/index_front_admin',$data);
	}

	public function login() {
		$data['kategori'] = $this->m_kategori->getAllData();		
		
		if(!empty($this->input->post()))
	    {
					
			$data['post'] = array($_POST['user_name'], $_POST['user_password']);
			
			if(!empty($data['post'][0]) and !empty($data['post'][1]))
			{	

				$user = $this->m_user->getUserLogin($data['post']);		

				if(!empty($user))
				{
					$role = $this->m_user->getUserLogin($data['post']);				
					
					if(!empty($role))
					{
						foreach ($role as $key => $value) {
							$sess_array[$key] = $value;
						}

						$this->session->set_userdata('logged_in', $sess_array);

						//redirect('c_f_home', 'refresh');
						redirect('c_user/profil', $data);
					}
					else
					{
						$data['msg'] = 'Password Salah';
					}
				}
				else
				{
					$data['msg'] = 'User Tidak Ditemukan';
				}
			}
			else
			{
				$data['msg'] = 'Username dan Password harus diisi';
			}
	    }

		$this->load->view('front/index_front',$data);

		
	}

	public function loginadmin() {
		$data['kategori'] = $this->m_kategori->getAllData();		
		
		if(!empty($this->input->post()))
	    {
					
			$data['post'] = array($_POST['user_name'], $_POST['user_password']);
			
			if(!empty($data['post'][0]) and !empty($data['post'][1]))
			{	

				$user = $this->m_user->getUserLoginAdmin($data['post']);		

				if(!empty($user))
				{
					$role = $this->m_user->getUserLoginAdmin($data['post']);				
					
					if(!empty($role))
					{
						foreach ($role as $key => $value) {
							$sess_array[$key] = $value;
						}

						$this->session->set_userdata('logged_in', $sess_array);

						//redirect('c_f_home', 'refresh');
						redirect('c_user/profil', $data);
					}
					else
					{
						$data['msg'] = 'Password Salah';
					}
				}
				else
				{
					$data['msg'] = 'User Tidak Ditemukan';
				}
			}
			else
			{
				$data['msg'] = 'Username dan Password harus diisi';
			}
	    }

		$this->load->view('front/index_front_admin',$data);

		
	}

	public function beli() {
		$data['kategori'] = $this->m_kategori->getAllData();
		
		$data['list'] = $this->m_barang->getAllData();
		
		if (empty($_POST['id_pembeli'])) {
			
			$data['msg'] = 'Anda Harus Login';

		}  else {		
			if (!empty($_POST['id_penjual'])) {
				if ($this->session->userdata['logged_in']['id'] == $_POST['id_penjual']) {
					$data['msg'] = 'Anda Tidak Bisa Membeli Barang Anda Sendiri';
				} else {
					$this->m_order->inputData();	
				
					$data['msg'] = 'Penambahan Order Berhasil';
				}
			} else {
				$data['msg'] = 'Penambahan Order Berhasil';
			}
		}


		$this->load->view('front/index_front',$data);
	}

	public function belipaket() {
		$data['kategori'] = $this->m_kategori->getAllData();
		
		$data['list'] = $this->m_barang->getAllData();
		
		if (empty($_POST['id_pembeli'])) {
			
			$data['msg'] = 'Anda Harus Login';

		}  else {		
			
			$this->m_orderpaket->inputData();	
			
			$data['msg'] = 'Penambahan Order Berhasil';
		}

		$this->load->view('front/index_front',$data);
	}


	public function kategori($id) {
		$data['msg'] = '';

		$data['list'] = $this->m_barang->getAllDataByIDKategori($id);

		$data['kategori'] = $this->m_kategori->getAllData();

		$data['list_kategori'] = $this->m_kategori->getAllData();

		

		$this->load->view('front/index_front_kategori',$data);
	}

	public function paket() {
		$data['msg'] = '';

		$data['kategori'] = $this->m_kategori->getAllData();
		$data['list_kategori'] = $this->m_kategori->getAllData();

		

		$data['msg'] = '';
		if (!empty($_POST['cari'])) {
			$data['list'] = $this->m_paket->getAllDatabysearch($_POST['cari']);
		} else {
			$data['list'] = $this->m_paket->getAllData();
		}
		
		
		$this->load->view('front/index_front_paket',$data);
	}

	
}
