<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_kategori extends CI_Controller {

	var $data = array();

	function __construct() {
		parent::__construct();
		
		// profiler
		// $this->output->enable_profiler($this->config->item('profiler_status'));

		/*
		if(!$this->session->userdata('ptbr_admin')) {
			redirect('auth');
		}
		else {
			$adminData = $this->session->userdata('ptbr_admin');
			$this->data['admin_name'] = $adminData['name'];
			$this->data['admin_photo'] = $adminData['photo'];
			$this->data['admin_lastLogin'] = $adminData['lastLogin'];
			$this->data['admin_kategoriName'] = $adminData['kategoriName'];

			$this->load->model('bon_sopir_model', 'bon_sopir_model');
			$this->load->model('pegawai_model', 'pegawai_model');
		}
		*/

		## load model here 
		$this->load->model('kategori_model', 'm_kategori');
	}

	public function index()	{
		$data = $this->data;

		$data['list'] = $this->m_kategori->getAllData();

		$this->load->view('kategori_data', $data);
	}

	public function submitAddData() {
		$this->m_kategori->inputData();

		$this->session->set_flashdata('input_success', 'Input data bon berhasil.');

		redirect('c_kategori');
	}

	public function edit($id) {
		$data = $this->data;

		if (!empty($id)) {
			$data['list_edit'] = $this->m_kategori->getAllDataByID($id) ;
		} 
		
	    $this->output->set_content_type('application/json');
	    
	    $this->output->set_output(json_encode($data));

	    return $data;
	}

	public function submitEditData() {
		$this->m_kategori->editData($this->input->post('id'));

		$this->session->set_flashdata('input_success', 'Ubah data berhasil.');

		redirect('c_kategori');
	}

	public function delete($id) {
		$reference = $this->m_kategori->deleteData($id);

		$this->session->set_flashdata('delete_success', 'Data berhasil dihapus.');

		redirect('c_kategori');
	}
}
