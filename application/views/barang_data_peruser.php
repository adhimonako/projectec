<!DOCTYPE html>
<html>
<head>
	<title>Welcome</title>

	<?php include 'header.php'; ?>
</head>
<body class="header-dark sidebar-light sidebar-expand">
	<?php include 'navbar.php'; ?>
	<?php include 'sidebar.php'; ?>

	<main class="main-wrapper clearfix">
		<div class="row page-title clearfix">
		    <div class="page-title-left">
		        <h6 class="page-title-heading mr-0 mr-r-5">Jasa Menu</h6>
		    </div>
		</div>

		<div class="widget-list">
            <div class="row">
                <div class="col-md-12 widget-holder">
                    <div class="widget-bg">
                    	<div class="widget-body clearfix">
                    		<div class=" col-md-2">
                    			<button class="btn btn-block btn-success ripple" data-toggle="modal" data-target="#modalcreate">Tambah</button>
                    		</div>
                            <table class="table table-striped table-responsive" data-toggle="datatables">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Nama</th>
                                        <th>Disk</th>
                                        <th>Action</th>
                                	</tr>
                                </thead>
                                <tbody>
                                	<?php $i = 1 ; ?>
                                	<?php foreach ($list as $row ) { ?>
                                		<tr>
	                                        <td><?php echo $i++; ?></td>
	                                        <td><?php echo ucwords($row->barang_name); ?></td>
	                                        <td><?php echo ucwords($row->barang_desc); ?></td>
	                                        <th>
	                                        	<button class="btn btn-sm btn-outline-default ripple btn-info" onclick="editdata(<?php echo $row->id;?>);">Edit</button>
	                                        	<button class="btn btn-sm btn-outline-default ripple btn-danger" onclick="deletedata(<?php echo $row->id;?>);">Delete</button>
	                                        </th>
	                                    </tr>
                                	<?php } ?>
                                	
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th>No</th>
                                        <th>Jasa</th>
                                        <th>Email</th>
                                        <th>Action</th>
                                	</tr>
                                </tfoot>
                            </table>
                        </div>
                        
                        <!-- /.widget-body -->
                    </div>
                    <!-- /.widget-bg -->
                </div>
            </div>
        </div>
	</main>

	<div class="modal modal-primary fade bs-modal-lg-primary" tabindex="-1" barang="dialog" id="modalcreate" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header text-inverse">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="myLargeModalLabel">Tambah Data</h5>
                </div>
                <div class="modal-body">
                    <?php echo form_open_multipart('c_barang/submitAddDataperUser'); ?>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label">Nama Jasa</label>
                            <div class="col-md-9">
                                <input class="form-control" type="text" name="barang_name">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label">Harga Jasa</label>
                            <div class="col-md-9">
                                <input class="form-control" type="number" name="barang_price">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label">Alamat Jasa</label>
                            <div class="col-md-9">
                                <input class="form-control" type="text" name="barang_region">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label">Disk Jasa</label>
                            <div class="col-md-9">
                                <input class="form-control" type="text" name="barang_desc">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label">Kategori</label>
                            <div class="col-md-9">
                                <select class="form-control" name="id_kategori">
                                	<?php foreach ($list_kategori as $key => $kat) { ?>
                                		<option value="<?php echo $kat->id; ?>"><?php echo $kat->kategori_name; ?></option>
                                	<?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-actions">
                            <div class="form-group row">
                                <div class="col-md-9 ml-md-auto btn-list">
                                    <input type="hidden" name="id_penjual" value="<?php echo $this->session->userdata['logged_in']['id'] ?>">
                                    <button class="btn btn-primary" type="Submit">Submit</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

    <div class="modal modal-primary fade bs-modal-lg-primary" tabindex="-1" barang="dialog" id="modaledit" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header text-inverse">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="myLargeModalLabel">Edit Data</h5>
                </div>
                <div class="modal-body">
                    <?php echo form_open_multipart('c_barang/submitEditDataperuser'); ?>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label">Nama Jasa</label>
                            <div class="col-md-9">
                                <input class="form-control" type="text" name="barang_name" id="barang_name">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label">Harga Jasa</label>
                            <div class="col-md-9">
                                <input class="form-control" type="number" name="barang_price" id="barang_price">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label">Alamat Jasa</label>
                            <div class="col-md-9">
                                <input class="form-control" type="text" name="barang_region" id="barang_region">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label">Disk Jasa</label>
                            <div class="col-md-9">
                                <input class="form-control" type="text" name="barang_desc" id="barang_desc">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label">Kategori</label>
                            <div class="col-md-9">
                                <select class="form-control" name="id_kategori" id="id_kategori">
                                	<?php foreach ($list_kategori as $key => $kat) { ?>
                                		<option value="<?php echo $kat->id; ?>"><?php echo $kat->kategori_name; ?></option>
                                	<?php } ?>
                                </select>
                            </div>
                        </div>

                         <div class="form-group row">
                            <label class="col-md-3 col-form-label">Photo Jasa</label>
                            <div class="col-md-9">
                                <input type="file" name="photo_barang">
                            </div>
                        </div>

                        <?php for ($i=1; $i < 5 ; $i++) {  ?>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label">Photo Jasa</label>
                            <div class="col-md-9">
                                <input type="file" name="<?php echo 'photo_barang'.$i; ?>">
                            </div>
                        </div>
                        <?php } ?>


                        <div class="form-actions">
                            <div class="form-group row">
                                <div class="col-md-9 ml-md-auto btn-list">
                                	<input type="hidden" name="id" id="id">
                                    <button class="btn btn-primary" type="Submit">Submit</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

	<?php include 'footer.php'; ?>
	<script type="text/javascript">
		var url="<?php echo site_url();?>";

		function editdata(id) {
	       $.ajax({
	        url: '<?php echo site_url('c_barang/edit'); ?>/'+id,
	        type:'GET',
	        dataType: 'json',
	        success: function(data){
	          // console.log(data);
	          $("#id").val(data['list_edit'][0]['id']);
	          $("#barang_name").val(data['list_edit'][0]['barang_name']);
	          $("#barang_price").val(data['list_edit'][0]['barang_price']);
	          $("#barang_region").val(data['list_edit'][0]['barang_region']);
	          $("#barang_desc").val(data['list_edit'][0]['barang_desc']);
	          $("#id_penjual").val(data['list_edit'][0]['id_penjual']);
	          $("#id_kategori").val(data['list_edit'][0]['id_kategori']);
	          $("#modaledit").modal('show');
	        }, 
	        error: function(){}
	      }); 
	    }

	    function deletedata(id){
	       var r=confirm("Apakah Data dengan ID "+id+" ingin di Hapus?")
	        if (r==true)
	          window.location = url+"/c_barang/deleteperuser/"+id;
	        else
	          return false;
	    }
	</script>
</body>
</html>