<?php if (!empty($this->session->userdata)) {
//print_r($this->session->userdata);die();
} ?>
<!DOCTYPE html>
<html lang="en">
<?php include 'headerfront.php'; ?>
<style type="text/css">
	.modal-backdrop {
		z-index: -1;
	}

	.single_product_thumbnails ul li {
		height: 80px;	
	}
</style>

<body>
<div class="modal fade" id="status" style="z-index: 1001;">
  <div class="modal-dialog">
    <div class="modal-content">
    	
			<!-- Modal Header -->
			<div class="modal-header">
				<h5 class="modal-title">Pemberitahuan</h5>
				<button type="button" class="close" data-dismiss="modal">&times;</button>
			</div>

			<!-- Modal body -->
			<div class="modal-body">
				<h4 style="color: #fe4c50;" id="mesagge_error">Pendaftaran Berhasil</h4>
			</div>
    	
    </div>
  </div>
</div>

<div class="super_container">

	<!-- Header -->

	<header class="header trans_300">

		<!-- Top Navigation -->

		<div class="top_nav">
			<div class="container">
				<div class="row">
					<div class="col-md-6">
						<!--
						<div class="top_nav_left">free shipping on all u.s orders over $50</div>
						-->
					</div>
					<div class="col-md-6 text-right">
						<div class="top_nav_right">
							<ul class="top_nav_menu">
								<li class="account">
									<a href="#">
										My Account
										<i class="fa fa-angle-down"></i>
									</a>
									
									<?php if (empty($this->session->userdata['logged_in']['id'])) { ?>
										<ul class="account_selection">										
											<li><a data-toggle="modal" data-target="#login"><i class="fa fa-sign-in" aria-hidden="true"></i>Sign In</a></li>

											<div class="modal fade" id="login" style="z-index: 1001;">
											  <div class="modal-dialog">
											    <div class="modal-content">
											    	<?php echo form_open_multipart('c_f_home/login'); ?>
														<!-- Modal Header -->
														<div class="modal-header">
															<h4 class="modal-title">Login</h4>
															<button type="button" class="close" data-dismiss="modal">&times;</button>
														</div>

														<!-- Modal body -->
														<div class="modal-body">
															<input class="form_input" type="text" name="user_name" placeholder="Nama" required="required" data-error="Name is required." maxlength="50">							
															<input class="form_input" type="password" name="user_password" placeholder="Password" required="required" data-error="Password is required." maxlength="50">	
														</div>

														<!-- Modal footer -->
														<div class="modal-footer">
															<input type="submit" class="message_submit_btn">
														</div>
											    	</form>  
											    </div>
											  </div>
											</div>

											<li><a data-toggle="modal" data-target="#register"><i class="fa fa-user-plus" aria-hidden="true"></i>Register</a></li>
											<div class="modal fade" id="register" style="z-index: 1000;">
											  <div class="modal-dialog">
											    <div class="modal-content">
											    	<?php echo form_open_multipart('c_f_home/register'); ?>
														<!-- Modal Header -->
														<div class="modal-header">
															<h4 class="modal-title">Register</h4>
															<button type="button" class="close" data-dismiss="modal">&times;</button>
														</div>

														<!-- Modal body -->
														<div class="modal-body">
															<input class="form_input" type="text" name="user_name" placeholder="Nama" required="required" data-error="Name is required." maxlength="50">							
															<input class="form_input" type="text" name="user_address" placeholder="Alamat" required="required" data-error="Alamat is required." maxlength="50">						
															<input class="form_input" type="number" name="user_phone" placeholder="No Telp" required="required" data-error="Telp is required." maxlength="50">			
															<input class="form_input" type="text" name="user_mail" placeholder="Email" required="required" data-error="Mail is required." maxlength="50">		
															<input class="form_input" type="password" name="user_password" placeholder="Password" required="required" data-error="Password is required." maxlength="50">	
															<select class="form_input" name="user_roleid" id="role">
																<option value="1" disabled>--Pilih Jenis--</option>
																<option value="2">Member</option>
																<option value="3">Vendor</option>																
															</select>	
															<div class="penjual">
																<input class="form_input" type="text" name="user_bussiness" placeholder="Nama Bisnis" data-error="Nama Bisnis is required." maxlength="50">			
																<input class="form_input" type="number" name="user_banknumber" placeholder="No rekening" data-error="No Rek is required." maxlength="50">	
															</div>
														</div>

														<!-- Modal footer -->
														<div class="modal-footer">
															<input type="submit"  class="newsletter_submit_btn message_submit_btn trans_300" >
														</div>
											    	</form>  
											    </div>
											  </div>
											</div>

										</ul>
									<?php } else { ?>
										<ul class="account_selection">
											<li><a href="<?php echo site_url('c_user/profil');?>"><i class="fa fa-user-plus" aria-hidden="true"></i>Profil</a></li>
											<li><a href="<?php echo site_url('c_user/logout');?>"><i class="fa fa-user-plus" aria-hidden="true"></i>Log Out</a></li>
										</ul>
									<?php } ?>
									

									
								</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>										

		<div class="main_nav_container">
			<div class="container">
				<div class="row">
					<div class="col-lg-12 text-right">
						<div class="logo_container">
							<a href="<?php echo site_url('c_f_home/'); ?>">Buka<span>Terop.com</span></a>
						</div>
						<nav class="navbar">
							<ul class="navbar_menu">
								
								<li><a href="<?php echo site_url('c_f_home/paket/'); ?>">Paket</a></li>

								<li class="currency" style="background: none;padding-bottom: 37px;border-right-width: 0px;">
									<a href="#">
										Kategori
										<i class="fa fa-angle-down"></i>
									</a>
									<ul class="currency_selection">
										<?php if (!empty($kategori)) { ?>
											<?php foreach ($kategori as $key => $kategori) { ?>
												<li><a href="<?php echo site_url('c_f_home/kategori/'.$kategori->id); ?>"><?php echo $kategori->kategori_name; ?></a></li>
											<?php } ?>
										<?php } ?>
									</ul>
								</li>
							</ul>
							<ul class="navbar_user">
								<li class="checkout">
									
									<?php if (!empty($this->session->userdata['logged_in'])) { ?>
										<a href="<?php echo site_url('c_order/pesananpembeli');?>">
											<i class="fa fa-shopping-cart" aria-hidden="true"></i>
										</a>
									<?php } else { ?>
										<a data-toggle="modal" data-target="#haruslogin">
											<i class="fa fa-shopping-cart" aria-hidden="true"></i>
										</a>	
									<?php } ?>
								</li>
							</ul>
							<div class="hamburger_container">
								<i class="fa fa-bars" aria-hidden="true"></i>
							</div>
						</nav>
					</div>
				</div>
			</div>
		</div>

	</header>

	<div class="fs_menu_overlay"></div>

	<div class="container product_section_container" style="margin-top: 250px;">
		<div class="row" style="margin-bottom:100px;">
			<div class="col product_section clearfix">

				<!-- Sidebar -->

				<div class="sidebar">
					<div class="sidebar_section">
						<div class="sidebar_title">
							<h3 style="color: #fe4c50;"><?php echo ucwords($kategori->kategori_name); ?></h3>
							<h5>Product Category</h5>
						</div>

					
						<ul class="sidebar_categories">
							
							<?php foreach ($list_kategori as $key => $kategori) { ?>
								<li><a href="<?php echo site_url('c_f_home/kategori/'.$kategori->id); ?>"><?php echo $kategori->kategori_name; ?></a></li>
							<?php } ?>
							<li><a href="<?php echo site_url('c_f_home/paket/'); ?>">Paket</a></li>
						</ul>
					</div>
				</div>

				<!-- Main Content -->

				<div class="main_content">

					<!-- Products -->

					<div class="products_iso">
						<div class="row">
							<div class="col">

								<!-- Product Sorting -->

								<div class="product_sorting_container product_sorting_container_top">
									<ul class="product_sorting">
										<li>
											<span class="type_sorting_text">Default Sorting</span>
											<i class="fa fa-angle-down"></i>
											<ul class="sorting_type">
												<li class="type_sorting_btn" data-isotope-option='{ "sortBy": "original-order" }'><span>Default Sorting</span></li>
												<li class="type_sorting_btn" data-isotope-option='{ "sortBy": "price" }'><span>Price</span></li>
												<li class="type_sorting_btn" data-isotope-option='{ "sortBy": "name" }'><span>Product Name</span></li>
											</ul>
										</li>
										<li>
											<span>Show</span>
											<span class="num_sorting_text">6</span>
											<i class="fa fa-angle-down"></i>
											<ul class="sorting_num">
												<li class="num_sorting_btn"><span>6</span></li>
												<li class="num_sorting_btn"><span>12</span></li>
												<li class="num_sorting_btn"><span>24</span></li>
											</ul>
										</li>
									</ul>
									<div class="pages d-flex flex-row align-items-center">
										<div class="page_current">
											<span>1</span>
											<ul class="page_selection">
												<li><a href="#">1</a></li>
												<li><a href="#">2</a></li>
												<li><a href="#">3</a></li>
											</ul>
										</div>
										<div class="page_total"><span>of</span> 3</div>
										<div id="next_page" class="page_next"><a href="#"><i class="fa fa-long-arrow-right" aria-hidden="true"></i></a></div>
									</div>

								</div>
									<!-- Product Grid -->

								<div class="product-grid">

									<?php foreach ($list as $key => $row) { ?>
										<a onclick="editdata(<?php echo $row->id;?>);">
											<div class="product-item men">
												<div class="product discount product_filter">
													<div class="product_image">
														<img style="width: 219px;" src="<?php echo base_url().'upload/barang/'.$row->photo_barang;?>" alt="">
													</div>
													<div class="favorite favorite_left"></div>
													<div class="product_bubble product_bubble_right product_bubble_red d-flex flex-column align-items-center"><span>New</span></div>
													<div class="product_info">
														<h6 class="product_name"><?php echo $row->barang_name ?></h6>
														<div class="product_price">Rp <?php echo number_format($row->barang_price); ?></div>
													</div>
												</div>
											</div>
										</a>
									<?php } ?>
										
									</div>	

									<!-- Product Sorting -->

									<div class="product_sorting_container product_sorting_container_bottom clearfix">
										<ul class="product_sorting">
											<li>
												<span>Show:</span>
												<span class="num_sorting_text">04</span>
												<i class="fa fa-angle-down"></i>
												<ul class="sorting_num">
													<li class="num_sorting_btn"><span>01</span></li>
													<li class="num_sorting_btn"><span>02</span></li>
													<li class="num_sorting_btn"><span>03</span></li>
													<li class="num_sorting_btn"><span>04</span></li>
												</ul>
											</li>
										</ul>
										<span class="showing_results">Showing 1–3 of 12 results</span>
										<div class="pages d-flex flex-row align-items-center">
											<div class="page_current">
												<span>1</span>
												<ul class="page_selection">
													<li><a href="#">1</a></li>
													<li><a href="#">2</a></li>
													<li><a href="#">3</a></li>
												</ul>
											</div>
											<div class="page_total"><span>of</span> 3</div>
											<div id="next_page_1" class="page_next"><a href="#"><i class="fa fa-long-arrow-right" aria-hidden="true"></i></a></div>
										</div>

									</div>

								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- Footer -->

		<div class="modal fade bd-example-modal-lg" id="detail" style="z-index: 1002;">
		  <div class="modal-dialog modal-lg">
		    <div class="modal-content">
		    	<?php echo form_open_multipart('c_f_home/beli'); ?>
					<!-- Modal Header -->
					<div class="modal-header">
						<h4 class="modal-title">Detail</h4>
						<button type="button" class="close" data-dismiss="modal">&times;</button>
					</div>

					<!-- Modal body -->
					<div class="modal-body">
						<div class="container single_product_container" style="margin-top: 0px !important;">
							<div class="row">
								<div class="col-lg-7">
									<div class="single_product_pics">
										<div class="row">
											<div class="col-lg-3 thumbnails_col order-lg-1 order-2">
												<div class="single_product_thumbnails">
													<ul>
														<li class=""><img src="images/single_1_thumb.jpg" alt="" data-image="images/single_1.jpg" id="myimage1"></li>
														<li class=""><img src="images/single_2_thumb.jpg" alt="" data-image="images/single_2.jpg" id="myimage2"></li>
														<li class=""><img src="images/single_3_thumb.jpg" alt="" data-image="images/single_3.jpg" id="myimage3"></li>
														<li class=""><img src="images/single_3_thumb.jpg" alt="" data-image="images/single_4.jpg" id="myimage4"></li>
													</ul>
												</div>
											</div>
											<div class="col-lg-9 image_col order-lg-2 order-1">
												<div class="single_product_image">
													<div class="single_product_image_background">
														<img style="max-width: 310px;" src="<?php //echo base_url().'upload/barang/'.$row->photo_barang;?>" alt="" id="myimage">
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>


								<div class="col-lg-5">
									<div class="product_details">
										<div class="product_details_title">
											<h2><span id="product_name_d"></span></h2>
											<p><span id="product_desc_d"></span></p>
										</div>
										<div class="free_delivery d-flex flex-row align-items-center justify-content-center">
											<span class="ti-truck"></span><span>free delivery</span>
										</div>
										<div class="product_price">Rp <a id="product_price_d"></a></div>
									</div>
								</div>

								<div class="container" style="margin-top: 10px;margin-top: 27px;">
									<div class="row">
										<div class="col-md-3"><label style="padding-top: 18px;">Tanggal Acara</label></div>
										<div class="col-md-9"><input type="date" name="tanggalacara" class="form_input"></div>
									</div>
									<div class="row">
									<div class="col-md-3"><label style="padding-top: 18px;">Jam Mulai Acara</label></div>
									<div class="col-md-9"><input type="time" name="jammulai" class="form_input"></div>
								</div>
								<div class="row">
									<div class="col-md-3"><label style="padding-top: 18px;">Jam Selesai Acara</label></div>
									<div class="col-md-9"><input type="time" name="jamselesai" class="form_input"></div>
								</div>
									<div class="row">
										<div class="col-md-3"><label style="padding-top: 18px;">Lama Acara</label></div>
										<div class="col-md-9"><input type="text" name="lamaacara" class="form_input" placeholder="masukan dalam hari"></div>
									</div>
								</div>
							</div>
						</div>
					</div>

					<!-- Modal footer -->
					<div class="modal-footer">
						<input type="hidden" name="id_barang" id="id_barang">
						<input type="hidden" name="id_pembeli" id="id_pembeli" value="<?php echo !empty($this->session->userdata['logged_in']['id'])? $this->session->userdata['logged_in']['id'] : ''; ?>">
						<input type="submit"  class="newsletter_submit_btn message_submit_btn trans_300" value="Pesan" >
					</div>
		    	</form>  
		    </div>
		  </div>
		</div>

		<!--
		<footer class="footer">
			<div class="container">
				<div class="row">
					<div class="col-lg-6">
						<div class="footer_nav_container d-flex flex-sm-row flex-column align-items-center justify-content-lg-start justify-content-center text-center">
							<ul class="footer_nav">
								<li><a href="#">Blog</a></li>
								<li><a href="#">FAQs</a></li>
								<li><a href="contact.html">Contact us</a></li>
							</ul>
						</div>
					</div>
					<div class="col-lg-6">
						<div class="footer_social d-flex flex-row align-items-center justify-content-lg-end justify-content-center">
							<ul>
								<li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
								<li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
								<li><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
								<li><a href="#"><i class="fa fa-skype" aria-hidden="true"></i></a></li>
								<li><a href="#"><i class="fa fa-pinterest" aria-hidden="true"></i></a></li>
							</ul>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-12">
						<div class="footer_nav_container">
							<div class="cr">©2018 All Rights Reserverd. This template is made with <i class="fa fa-heart-o" aria-hidden="true"></i> by <a href="#">Colorlib</a></div>
						</div>
					</div>
				</div>
			</div>
		</footer>
		-->

		<div class="modal fade" id="haruslogin" style="z-index: 1000;">
		  <div class="modal-dialog">
		    <div class="modal-content">
		    	
					<!-- Modal Header -->
					<div class="modal-header">
						<h4 class="modal-title">Pemberitahuan</h4>
						<button type="button" class="close" data-dismiss="modal">&times;</button>
					</div>

					<!-- Modal body -->
					<div class="modal-body">
						<div> <h4><span id=""> Anda Harus Login</span></h4></div>
					</div>

		    	
		    </div>
		  </div>
		</div>

	</div>

	<?php include 'footerfront.php'; ?>

	<script type="text/javascript">

		$(document ).ready(function() {
			$(".penjual").hide();
			$('#role option[value=1]').attr('selected','selected');

			var data_error = '<?php echo $msg; ?>';
			if (data_error != '') {
				$("#mesagge_error").text(data_error);			
			    $("#status").modal('show');
		    }
		});

		$("#role").change(function() {
			if ($("#role").val() == '3') {
				$(".penjual").show();
			} else {
				$(".penjual").hide();	
			}
		});

		function editdata(id) {
	       $.ajax({
	        url: '<?php echo site_url('c_barang/edit'); ?>/'+id,
	        type:'GET',
	        dataType: 'json',
	        success: function(data){
	          $("#id_barang").val(data['list_edit'][0]['id']);
	          $("#product_name_d").text(data['list_edit'][0]['barang_name']);
	          $("#product_price_d").text(parseInt(data['list_edit'][0]['barang_price']).toLocaleString());
	          $("#product_desc_d").text(data['list_edit'][0]['barang_desc']);

	          $('#myimage').attr('src','<?php echo base_url()."upload/barang/"; ?>'+data['list_edit'][0]['photo_barang']);

				$('#myimage1').attr('src','<?php echo base_url()."upload/barang/"; ?>'+data['list_edit'][0]['photo_barang1']);
				$('#myimage2').attr('src','<?php echo base_url()."upload/barang/"; ?>'+data['list_edit'][0]['photo_barang2']);
				$('#myimage3').attr('src','<?php echo base_url()."upload/barang/"; ?>'+data['list_edit'][0]['photo_barang3']);
				$('#myimage4').attr('src','<?php echo base_url()."upload/barang/"; ?>'+data['list_edit'][0]['photo_barang4']);

	          $("#detail").modal('show');
	        }, 
	        error: function(){}
	      }); 
	    }

	</script>
</body>
</html>
