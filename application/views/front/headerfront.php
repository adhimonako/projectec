<head>
    <title></title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="Colo Shop Template">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/front/styles/bootstrap4/bootstrap.min.css';?>">
    <link href="<?php echo base_url().'assets/front/plugins/font-awesome-4.7.0/css/font-awesome.min.css';?>" rel="stylesheet" type="text/css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/front/plugins/OwlCarousel2-2.2.1/owl.carousel.css';?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/front/plugins/OwlCarousel2-2.2.1/owl.theme.default.css';?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/front/plugins/OwlCarousel2-2.2.1/animate.css';?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/front/styles/main_styles.css';?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/front/styles/responsive.css';?>">

    <!-- category -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/front/plugins/jquery-ui-1.12.1.custom/jquery-ui.css';?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/front/styles/categories_styles.css';?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/front/styles/categories_responsive.css';?>">

    <!-- contact -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/front/styles/contact_styles.css';?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/front/styles/contact_responsive.css';?>">

    <!-- single -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/front/styles/single_styles.css';?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/front/styles/single_responsive.css';?>">

</head>