<?php 

//print_r($this->session->userdata['user_data']);die();
// if (!empty($this->session->userdata['user_data'])) {
// die('xxx');
// } ?>
<!DOCTYPE html>
<html lang="en">
<?php include 'headerfront.php'; ?>
<style type="text/css">
	.modal-backdrop {
		z-index: -1;
	}


	.single_product_thumbnails ul li {
		height: 80px;	
	}
</style>

<body>


<div class="super_container">

	<!-- Header -->

	<header class="header trans_300">

		<!-- Top Navigation -->

		<div class="top_nav">
			<div class="container">
				<div class="row">
					<div class="col-md-6">
						<div class="top_nav_left"></div>
					</div>
					<div class="col-md-6 text-right">
						<div class="top_nav_right">
							<ul class="top_nav_menu">
								<li class="account">
									<a href="#">
										My Account
										<i class="fa fa-angle-down"></i>
									</a>
									
									<?php if (empty($this->session->userdata['logged_in']['id'])) { ?>
										<ul class="account_selection">										
											<li><a data-toggle="modal" data-target="#login"><i class="fa fa-sign-in" aria-hidden="true"></i>Sign In</a></li>

											<div class="modal fade" id="login" style="z-index: 1001;">
											  <div class="modal-dialog">
											    <div class="modal-content">
											    	<?php echo form_open_multipart('c_f_home/loginadmin'); ?>
														<!-- Modal Header -->
														<div class="modal-header">
															<h4 class="modal-title">Login</h4>
															<button type="button" class="close" data-dismiss="modal">&times;</button>
														</div>

														<!-- Modal body -->
														<div class="modal-body">
															<input class="form_input" type="text" name="user_name" placeholder="Nama" required="required" data-error="Name is required." maxlength="50">							
															<input class="form_input" type="password" name="user_password" placeholder="Password" required="required" data-error="Password is required." maxlength="50">	
														</div>

														<!-- Modal footer -->
														<div class="modal-footer">
															<input type="submit" class="message_submit_btn">
														</div>
											    	</form>  
											    </div>
											  </div>
											</div>

											
											<div class="modal fade" id="register" style="z-index: 1000;">
											  <div class="modal-dialog">
											    <div class="modal-content">
											    	<?php echo form_open_multipart('c_f_home/register'); ?>
														<!-- Modal Header -->
														<div class="modal-header">
															<h4 class="modal-title">Register</h4>
															<button type="button" class="close" data-dismiss="modal">&times;</button>
														</div>

														<!-- Modal body -->
														<div class="modal-body">
															<input class="form_input" type="text" name="user_name" placeholder="Nama" required="required" data-error="Name is required." maxlength="50">							
															<input class="form_input" type="text" name="user_address" placeholder="Alamat" required="required" data-error="Alamat is required." maxlength="50">						
															<input class="form_input" type="number" name="user_phone" placeholder="No Telp" required="required" data-error="Telp is required." maxlength="50">			
															<input class="form_input" type="text" name="user_mail" placeholder="Email" required="required" data-error="Mail is required." maxlength="50">		
															<input class="form_input" type="password" name="user_password" placeholder="Password" required="required" data-error="Password is required." maxlength="50">	
															<select class="form_input" name="user_roleid" id="role">
																<option value="1" disabled>--Pilih Jenis--</option>
																<option value="2">Vendor</option>																
																<option value="3">Member</option>
															</select>	
															<div class="penjual">
																<input class="form_input" type="text" name="user_bussiness" placeholder="Nama Bisnis" data-error="Nama Bisnis is required." maxlength="50">			
																<input class="form_input" type="text" name="nama_bank" placeholder="Nama Bank" data-error="Nama Bank is required." maxlength="50">			
																<input class="form_input" type="number" name="user_banknumber" placeholder="No rekening" data-error="No Rek is required." maxlength="50">	
															</div>
														</div>

														<!-- Modal footer -->
														<div class="modal-footer">
															<input type="submit"  class="newsletter_submit_btn message_submit_btn trans_300" >
														</div>
											    	</form>  
											    </div>
											  </div>
											</div>

										</ul>
									<?php } else { ?>
										<ul class="account_selection">
											<li><a href="<?php echo site_url('c_user/profil');?>"><i class="fa fa-user-plus" aria-hidden="true"></i>Profil</a></li>
											<li><a href="<?php echo site_url('c_user/logout');?>"><i class="fa fa-user-plus" aria-hidden="true"></i>Log Out</a></li>
											
											
										</ul>
									<?php } ?>
									

									
								</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>										

	</header>


	<div class="modal fade" id="status" style="z-index: 1000;">
	  <div class="modal-dialog">
	    <div class="modal-content">
	    	
				<!-- Modal Header -->
				<div class="modal-header">
					<h4 class="modal-title">Pemberitahuan</h4>
					<button type="button" class="close" data-dismiss="modal">&times;</button>
				</div>

				<!-- Modal body -->
				<div class="modal-body">
					<div> <h4><span id="mesagge_error"></span></h4></div>
				</div>

	    	
	    </div>
	  </div>
	</div>

	<div class="modal fade" id="haruslogin" style="z-index: 1000;">
	  <div class="modal-dialog">
	    <div class="modal-content">
	    	
				<!-- Modal Header -->
				<div class="modal-header">
					<h4 class="modal-title">Pemberitahuan</h4>
					<button type="button" class="close" data-dismiss="modal">&times;</button>
				</div>

				<!-- Modal body -->
				<div class="modal-body">
					<div> <h4><span id=""> Anda Harus Login</span></h4></div>
				</div>

	    	
	    </div>
	  </div>
	</div>

	<?php include 'footerfront.php'; ?>

	<script type="text/javascript">

		$(document ).ready(function() {
			$(".penjual").hide();
			$('#role option[value=1]').attr('selected','selected');

			var data_error = '<?php echo $msg; ?>';
			if (data_error != '') {
				$("#mesagge_error").text(data_error);			
			    $("#status").modal('show');
		    }
		});

		$("#role").change(function() {
			if ($("#role").val() == '2') {
				$(".penjual").show();
			} else {
				$(".penjual").hide();	
			}
		});

		function reset() {
			window.location = url+"/c_f_home/";
		}


		function editdata(id) {
	       $.ajax({
	        url: '<?php echo site_url('c_barang/edit'); ?>/'+id,
	        type:'GET',
	        dataType: 'json',
	        success: function(data){
	        console.log(data);

	          $("#id_barang").val(data['list_edit'][0]['id']);
	          $("#product_name_d").text(data['list_edit'][0]['barang_name']);
	          $("#product_price_d").text(parseInt(data['list_edit'][0]['barang_price']).toLocaleString());
	          $("#product_desc_d").text(data['list_edit'][0]['barang_desc']);
	          $("#id_penjual").val(data['list_edit'][0]['id_penjual']);

	          

	          $('#myimage').attr('src','<?php echo base_url()."upload/barang/"; ?>'+data['list_edit'][0]['photo_barang']);

				$('#myimage1').attr('src','<?php echo base_url()."upload/barang/"; ?>'+data['list_edit'][0]['photo_barang1']);
				$('#myimage2').attr('src','<?php echo base_url()."upload/barang/"; ?>'+data['list_edit'][0]['photo_barang2']);
				$('#myimage3').attr('src','<?php echo base_url()."upload/barang/"; ?>'+data['list_edit'][0]['photo_barang3']);
				$('#myimage4').attr('src','<?php echo base_url()."upload/barang/"; ?>'+data['list_edit'][0]['photo_barang4']);
	          

	       

	          $("#detail").modal('show');
	        }, 
	        error: function(){}
	      }); 
	    }

	</script>
</body>
</html>
