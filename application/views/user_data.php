<!DOCTYPE html>
<html>
<head>
	<title>Welcome</title>

	<?php include 'header.php'; ?>
</head>
<body class="header-dark sidebar-light sidebar-expand">
	<?php include 'navbar.php'; ?>
	<?php include 'sidebar.php'; ?>

	<main class="main-wrapper clearfix">
		<div class="row page-title clearfix">
		    <div class="page-title-left">
		        <h6 class="page-title-heading mr-0 mr-r-5">User Menu</h6>
		    </div>
		</div>

		<div class="widget-list">
            <div class="row">
                <div class="col-md-12 widget-holder">
                    <div class="widget-bg">
                    	<div class="widget-body clearfix">
                    		<div class=" col-md-2">
                    			<button class="btn btn-block btn-success ripple" data-toggle="modal" data-target="#modalcreate">Tambah</button>
                    		</div>
                            <table class="table table-striped table-responsive" data-toggle="datatables">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>User</th>
                                        <th>Email</th>
                                        <th>Status</th>
                                        <th>Action</th>
                                	</tr>
                                </thead>
                                <tbody>
                                	<?php $i = 1 ; ?>
                                	<?php foreach ($list as $row ) { ?>
                                		<tr>
	                                        <td><?php echo $i++; ?></td>
	                                        <td><?php echo ucwords($row->user_name); ?></td>
	                                        <td><?php echo ucwords($row->user_mail); ?></td>
                                            <td><?php echo ucwords($row->user_status_langganan != '1'? "Belum Aktif" : 'Sudah Aktif'); ?></td>
	                                        <th>
	                                        	<button class="btn btn-sm btn-outline-default ripple btn-info" onclick="editdata(<?php echo $row->id;?>);">Edit</button>
	                                        	<button class="btn btn-sm btn-outline-default ripple btn-danger" onclick="deletedata(<?php echo $row->id;?>);">Delete</button>
	                                        </th>
	                                    </tr>
                                	<?php } ?>
                                	
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th>No</th>
                                        <th>User</th>
                                        <th>Email</th>
                                        <th>Status</th>
                                        <th>Action</th>
                                	</tr>
                                </tfoot>
                            </table>
                        </div>
                        
                        <!-- /.widget-body -->
                    </div>
                    <!-- /.widget-bg -->
                </div>
            </div>
        </div>
	</main>

	<div class="modal modal-primary fade bs-modal-lg-primary" tabindex="-1" user="dialog" id="modalcreate" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header text-inverse">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="myLargeModalLabel">Tambah Data</h5>
                </div>
                <div class="modal-body">
                    <?php echo form_open_multipart('c_user/submitadddata'); ?>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label">Nama User</label>
                            <div class="col-md-9">
                                <input class="form-control" type="text" name="user_name">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label">Phone User</label>
                            <div class="col-md-9">
                                <input class="form-control" type="number" name="user_phone">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label">Alamat User</label>
                            <div class="col-md-9">
                                <input class="form-control" type="text" name="user_address">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label">Email User</label>
                            <div class="col-md-9">
                                <input class="form-control" type="email" name="user_mail">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label">Password User</label>
                            <div class="col-md-9">
                                <input class="form-control" type="password" name="user_password">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label">Role User</label>
                            <div class="col-md-9">
                                <select class="form-control" name="user_roleid">
                                	<?php foreach ($list_role as $key => $role) { ?>
                                		<option value="<?php echo $role->id; ?>"><?php echo $role->role_name; ?></option>
                                	<?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-actions">
                            <div class="form-group row">
                                <div class="col-md-9 ml-md-auto btn-list">
                                    <button class="btn btn-primary" type="Submit">Submit</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

    <div class="modal modal-primary fade bs-modal-lg-primary" tabindex="-1" user="dialog" id="modaledit" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header text-inverse">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="myLargeModalLabel">Edit Data</h5>
                </div>
                <div class="modal-body">
                    <?php echo form_open_multipart('c_user/submiteditdata'); ?>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label">Nama User</label>
                            <div class="col-md-9">
                                <input class="form-control" type="text" name="user_name" id="user_name">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label">Phone User </label>
                            <div class="col-md-9">
                                <input class="form-control" type="number" name="user_phone" id="user_phone">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label">Alamat User </label>
                            <div class="col-md-9">
                                <input class="form-control" type="text" name="user_address" id="user_address">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label">Email User </label>
                            <div class="col-md-9">
                                <input class="form-control" type="email" name="user_mail" id="user_mail">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label">Password User </label>
                            <div class="col-md-9">
                                <input class="form-control" type="password" name="user_password" id="user_password">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label">Role User </label>
                            <div class="col-md-9">
                                <select class="form-control" name="user_roleid" id="user_roleid">
                                	<?php foreach ($list_role as $key => $role) { ?>
                                		<option value="<?php echo $role->id; ?>"><?php echo $role->role_name; ?></option>
                                	<?php } ?>
                                </select>
                            </div>
                        </div>
                        
                        <div id="x">
                             <div class="form-group row">
                                <label class="col-md-3 col-form-label">Nama Bisnis</label>
                                <div class="col-md-9">
                                    <input class="form-control" type="text" name="user_bussiness" id="user_bussiness">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-md-3 col-form-label">Account Bank Number</label>
                                <div class="col-md-9">
                                    <input class="form-control" type="text" name="user_banknumber" id="user_banknumber">
                                </div>
                            </div>
                            
                            <div class="form-group row">
                                <label class="col-md-3 col-form-label">Status</label>
                                <div class="col-md-9">
                                    <a id="linkdownloadbukti">Download Bukti</a>
                                    <select class="form-control" name="user_status_langganan" id="user_status_langganan">
                                        <option value="1">Aktif</option>
                                        <option value="2">Non Aktif</option>                                    
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="form-actions">
                            <div class="form-group row">
                                <div class="col-md-9 ml-md-auto btn-list">
                                	<input type="hidden" name="id" id="id">
                                    <button class="btn btn-primary" type="Submit">Submit</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

	<?php include 'footer.php'; ?>
	<script type="text/javascript">
		var url="<?php echo base_url();?>";

		function editdata(id) {
	       $.ajax({
	        url: '<?php echo site_url('c_user/edit'); ?>/'+id,
	        type:'GET',
	        dataType: 'json',
	        success: function(data){
	          // console.log(data);
	          $("#id").val(data['list_edit'][0]['id']);
	          $("#user_name").val(data['list_edit'][0]['user_name']);
	          $("#user_phone").val(data['list_edit'][0]['user_phone']);
	          $("#user_address").val(data['list_edit'][0]['user_address']);
	          $("#user_mail").val(data['list_edit'][0]['user_mail']);
	          $("#user_password").val(data['list_edit'][0]['user_password']);
	          $("#user_roleid").val(data['list_edit'][0]['user_roleid']);
	          $("#user_bussiness").val(data['list_edit'][0]['user_bussiness']);
	          $("#user_banknumber").val(data['list_edit'][0]['user_banknumber']);
              $("#user_status_langganan").val(data['list_edit'][0]['user_status_langganan']);

              $("#linkdownloadbukti").attr("href", url+"upload/buktilangganan/"+data['list_edit'][0]['user_photo_langganan']) ;

              
              if (data['list_edit'][0]['user_roleid'] != '2') {
                $('#x').css('display', 'none')
              }

	          $("#modaledit").modal('show');
	        }, 
	        error: function(){}
	      }); 
	    }

	    function deletedata(id){
            var url="<?php echo site_url();?>";

	        var r=confirm("Apakah Data dengan ID "+id+" ingin di Hapus?")
	        if (r==true)
	          window.location = url+"/c_user/delete/"+id;
	        else
	          return false;
	    }
	</script>
</body>
</html>