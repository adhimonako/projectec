<script src="<?php echo base_url().'assets/front/js/jquery-3.2.1.min.js';?>"></script>
<script src="<?php echo base_url().'assets/front/styles/bootstrap4/popper.js';?>"></script>
<script src="<?php echo base_url().'assets/front/styles/bootstrap4/bootstrap.min.js';?>"></script>
<script src="<?php echo base_url().'assets/front/plugins/Isotope/isotope.pkgd.min.js';?>"></script>
<script src="<?php echo base_url().'assets/front/plugins/OwlCarousel2-2.2.1/owl.carousel.js';?>"></script>
<script src="<?php echo base_url().'assets/front/plugins/easing/easing.js';?>"></script>
<script src="<?php echo base_url().'assets/front/js/custom.js';?>"></script>

<!-- category -->
<script src="<?php echo base_url().'assets/front/plugins/jquery-ui-1.12.1.custom/jquery-ui.js';?>"></script>
<script src="<?php echo base_url().'assets/front/js/categories_custom.js';?>"></script>

<!-- contact -->
<script src="<?php echo base_url().'assets/front/js/contact_custom.js';?>"></script>

<!-- single -->
<script src="<?php echo base_url().'assets/front/js/single_custom.js';?>"></script>