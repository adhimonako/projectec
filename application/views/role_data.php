<!DOCTYPE html>
<html>
<head>
	<title>Welcome</title>

	<?php include 'header.php'; ?>
</head>
<body class="header-dark sidebar-light sidebar-expand">
	<?php include 'navbar.php'; ?>
	<?php include 'sidebar.php'; ?>

	<main class="main-wrapper clearfix">
		<div class="row page-title clearfix">
		    <div class="page-title-left">
		        <h6 class="page-title-heading mr-0 mr-r-5">Role Menu</h6>
		    </div>
		</div>

		<div class="widget-list">
            <div class="row">
                <div class="col-md-12 widget-holder">
                    <div class="widget-bg">
                    	<div class="widget-body clearfix">
                    		<div class=" col-md-2">
                    			<button class="btn btn-block btn-success ripple" data-toggle="modal" data-target="#modalcreate">Tambah</button>
                    		</div>
                            <table class="table table-striped table-responsive" data-toggle="datatables">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Role</th>
                                        <th>Disk</th>
                                        <th>Action</th>
                                	</tr>
                                </thead>
                                <tbody>
                                	<?php $i = 1 ; ?>
                                	<?php foreach ($list as $row ) { ?>
                                		<tr>
	                                        <td><?php echo $i++; ?></td>
	                                        <td><?php echo ucwords($row->role_name); ?></td>
	                                        <td><?php echo ucwords($row->role_desc); ?></td>
	                                        <th>
	                                        	<button class="btn btn-sm btn-outline-default ripple btn-info" onclick="editdata(<?php echo $row->id;?>);">Edit</button>
	                                        	<button class="btn btn-sm btn-outline-default ripple btn-danger" onclick="deletedata(<?php echo $row->id;?>);">Delete</button>
	                                        </th>
	                                    </tr>
                                	<?php } ?>
                                	
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th>No</th>
                                        <th>Role</th>
                                        <th>Disk</th>
                                        <th>Action</th>
                                	</tr>
                                </tfoot>
                            </table>
                        </div>
                        
                        <!-- /.widget-body -->
                    </div>
                    <!-- /.widget-bg -->
                </div>
            </div>
        </div>
	</main>

	<div class="modal modal-primary fade bs-modal-lg-primary" tabindex="-1" role="dialog" id="modalcreate" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header text-inverse">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="myLargeModalLabel">Tambah Data</h5>
                </div>
                <div class="modal-body">
                    <?php echo form_open_multipart('c_role/submitadddata'); ?>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label">Nama Role</label>
                            <div class="col-md-9">
                                <input class="form-control" type="text" name="role_name">
                            </div>
                        </div>
                      	<div class="form-group row">
                            <label class="col-md-3 col-form-label">Disk Role</label>
                            <div class="col-md-9">
                                <textarea class="form-control" rows="3" name="role_desc"></textarea>
                            </div>
                        </div>
                        <div class="form-actions">
                            <div class="form-group row">
                                <div class="col-md-9 ml-md-auto btn-list">
                                    <button class="btn btn-primary" type="Submit">Submit</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

    <div class="modal modal-primary fade bs-modal-lg-primary" tabindex="-1" role="dialog" id="modaledit" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header text-inverse">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="myLargeModalLabel">Edit Data</h5>
                </div>
                <div class="modal-body">
                    <?php echo form_open_multipart('c_role/submiteditdata'); ?>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label">Nama Role</label>
                            <div class="col-md-9">
                                <input class="form-control" type="text" name="role_name" id="role_name">
                            </div>
                        </div>
                      	<div class="form-group row">
                            <label class="col-md-3 col-form-label">Disk Role</label>
                            <div class="col-md-9">
                                <textarea class="form-control" rows="3" name="role_desc" id="role_desc"></textarea>
                            </div>
                        </div>
                        <div class="form-actions">
                            <div class="form-group row">
                                <div class="col-md-9 ml-md-auto btn-list">
                                	<input type="hidden" name="id" id="id">
                                    <button class="btn btn-primary" type="Submit">Submit</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

	<?php include 'footer.php'; ?>
	<script type="text/javascript">
		var url="<?php echo site_url();?>";

		function editdata(id) {
	       $.ajax({
	        url: '<?php echo site_url('c_role/edit'); ?>/'+id,
	        type:'GET',
	        dataType: 'json',
	        success: function(data){
	          // console.log(data);
	          $("#id").val(data['list_edit'][0]['id']);
	          $("#role_name").val(data['list_edit'][0]['role_name']);
	          $("#role_desc").val(data['list_edit'][0]['role_desc']);
	          $("#modaledit").modal('show');
	        }, 
	        error: function(){}
	      }); 
	    }

	    function deletedata(id){
	       var r=confirm("Apakah Data dengan ID "+id+" ingin di Hapus?")
	        if (r==true)
	          window.location = url+"/c_role/delete/"+id;
	        else
	          return false;
	    }
	</script>
</body>
</html>