<nav class="navbar">
    <!-- Logo Area -->
    <div class="navbar-header">

        <a href="<?php echo site_url('c_f_home');?>" class="navbar-brand">
            <!-- Disini -->
            <img class="logo-expand" alt="" src="<?php echo base_url().'assets/back/demo/users/Logo.png'; ?>">
            
            <!-- <p>BonVue</p> -->
        </a>
    </div>
    <!-- /.navbar-header -->
    <!-- Left Menu & Sidebar Toggle -->
    <ul class="nav navbar-nav">
        <li class="sidebar-toggle"><a href="javascript:void(0)" class="ripple"><i class="feather feather-menu list-icon fs-20"></i></a>
        </li>
    </ul>
    <!-- /.navbar-left -->
    <!-- Search Form -->
    
    <!-- /.navbar-search -->
    <div class="spacer"></div>
    <!-- Button: Create New -->
    <div class="btn-list dropdown d-none d-md-flex mr-4">
        <div class="dropdown-menu dropdown-left animated flipInY">
            <span class="dropdown-header">Create new ...</span> 
                 <a class="dropdown-item" href="#">Projects</a>  
                 <a class="dropdown-item" href="#">User Profile</a>  
                 <a class="dropdown-item" href="#">
                    <span class="d-flex align-items-end">
                        <span class="flex-1">To-do Item</span> 
                        <span class="badge badge-pill bg-primary-contrast">7</span> 
                    </span>
                </a>
                <a class="dropdown-item" href="#">
                <span class="d-flex align-items-end">
                    <span class="flex-1">Mail</span> 
                     <span class="badge badge-pill bg-color-scheme-contrast">23</span>
                 </span>
                </a>
                
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="#">
                    <span class="d-flex align-items-center">
                        <span class="flex-1">Settings</span> 
                            <i class="feather feather-settings list-icon icon-muted"></i>
                        </span>
                </a>
        </div>
    </div>
    <!-- /.btn-list -->
    <!-- User Image with Dropdown -->
    <ul class="nav navbar-nav">
        <!--
        <li class="dropdown">
            <a href="javascript:void(0);" class="dropdown-toggle ripple" data-toggle="dropdown">
                <span class="avatar thumb-xs2">
                    <img src="<?php echo base_url().'assets/back/demo/users/user1.jpg'; ?>" class="rounded-circle" alt=""> <i class="feather feather-chevron-down list-icon"></i>
                </span>
            </a>
            
            <div class="dropdown-menu dropdown-left dropdown-card dropdown-card-profile animated flipInY">
                <div class="card">
                    <header class="card-header d-flex mb-0"><a href="javascript:void(0);" class="col-md-4 text-center"><i class="feather feather-user-plus align-middle"></i> </a><a href="javascript:void(0);" class="col-md-4 text-center"><i class="feather feather-settings align-middle"></i> </a>
                        <a
                        href="javascript:void(0);" class="col-md-4 text-center"><i class="feather feather-power align-middle"></i>
                            </a>
                    </header>
                    <ul class="list-unstyled card-body">
                        <li><a href="#"><span><span class="align-middle">Manage Accounts</span></span></a>
                        </li>
                        <li><a href="#"><span><span class="align-middle">Change Password</span></span></a>
                        </li>
                        <li><a href="#"><span><span class="align-middle">Check Inbox</span></span></a>
                        </li>
                        <li><a href="#"><span><span class="align-middle">Sign Out</span></span></a>
                        </li>
                    </ul>
                </div>
            </div> 
        </li>
        -->
    </ul>
    <!-- /.navbar-right -->

    <!-- Right Menu -->


    <ul class="nav navbar-nav d-none d-lg-flex ml-2">
        <!-- /.dropdown -->
        <li><a href="javascript:void(0);" class="right-sidebar-toggle active ripple ml-3"><i class="feather feather-grid list-icon"></i></a>
        </li>
    </ul>
<!-- /.navbar-right -->
</nav>