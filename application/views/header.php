<!-- Mirrored from dharansh.in/bonvuehtml/default/ by HTTrack Website Copier/3.x [XR&CO'2014], Sat, 16 Dec 2017 20:57:40 GMT -->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1,shrink-to-fit=no">
    <link rel="icon" type="image/png" sizes="16x16" href="assets/demo/favicon.png">
    <link rel="stylesheet" href="<?php echo base_url().'assets/back/css/pace.css'; ?>">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Default</title>
    <!-- CSS -->

    <link href="<?php echo base_url().'assets/back/vendors/material-icons/material-icons.css'; ?>" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url().'assets/back/vendors/feather-icons/feather.css'; ?>" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url().'assets/back/css/style.css'; ?>" rel="stylesheet" type="text/css">

    <link href="<?php echo base_url().'assets/back/cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/magnific-popup.min.css';?> " rel="stylesheet" type="text/css">
    <link href="<?php echo base_url().'assets/back/cdnjs.cloudflare.com/ajax/libs/jquery.perfect-scrollbar/0.7.0/css/perfect-scrollbar.min.css';?>" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,500,600,700|Roboto:300,400" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url().'assets/back/cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css'; ?>" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url().'assets/back/cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick.min.css'; ?>" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url().'assets/back/cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick-theme.min.css'; ?>" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url().'assets/back/cdnjs.cloudflare.com/ajax/libs/switchery/0.8.2/switchery.min.css'; ?>" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url().'assets/back/cdnjs.cloudflare.com/ajax/libs/bootstrap-daterangepicker/2.1.25/daterangepicker.min.css'; ?>" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url().'assets/back/cdnjs.cloudflare.com/ajax/libs/datatables/1.10.15/css/jquery.dataTables.min.css';?>" rel="stylesheet" type="text/css">
    
    <!-- Head Libs -->
    <script src="<?php echo base_url().'assets/back/cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js';?>"></script>
    <script data-pace-options='{ "ajax": false }' src="<?php echo base_url().'assets/back/cdnjs.cloudflare.com/ajax/libs/pace/1.0.2/pace.min.js';?>"></script>
</head>