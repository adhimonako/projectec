<!DOCTYPE html>
<html>
<head>
	<title>Welcome</title>

	<?php include 'header.php'; ?>

    <style type="text/css">
        .modal-backdrop {
            z-index: -1;
        }

    </style>

</head>
<body class="header-dark sidebar-light sidebar-expand">
	
	

    <div class="modal fade" id="status" style="z-index: 1001;">
      <div class="modal-dialog">
        <div class="modal-content">
            
                <!-- Modal Header -->
                <div class="modal-header">
                    <h5 class="modal-title">Pemberitahuan</h5>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>

                <!-- Modal body -->
                <div class="modal-body">
                    <h4 style="color: #fe4c50;" id="mesagge_error">Pendaftaran Berhasil</h4>
                </div>
            
        </div>
      </div>
    </div>
    
    <div id="main1">
        <?php include 'navbar.php'; ?>
        <?php include 'sidebar.php'; ?>
    </div>

    <main class="main-wrapper clearfix" id="main">
        
		<div class="row page-title clearfix">
		    <div class="page-title-left">
		        <h6 class="page-title-heading mr-0 mr-r-5">Order Menu</h6>
		    </div>
		</div>

		<div class="widget-list">
            <div class="row">
                <div class="col-md-12 widget-holder">
                    <div class="widget-bg">
                    	<div class="widget-body clearfix">
                            <table class="table table-striped table-responsive" data-toggle="datatables">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Pembayaran</th>
                                        <th>Telp Vendor</th>
                                        <th>Tanggal Acara</th>
                                        <th>Nama Jasa</th>
                                        <th>Harga Jasa</th>
                                        <th>Lama Sewa</th>
                                        <th>Waktu</th>
                                        <th>Total Bayar</th>
                                        <th>Status Pesan</th>
                                        <th>Action</th>
                                	</tr>
                                </thead>
                                <tbody>
                                    <?php $i = 1 ; ?>   
                                    
                                	<?php foreach ($list as $row ) { ?>
                                		<tr>
	                                        <td><?php echo $i++; ?></td>
	                                        <td><?php echo 'Bayar ke Rek :<b>#'.$row->user_banknumber.'</b><br> Bank : <b>'.$row->nama_bank.'</b>' ; ?></td>
                                            <td><?php echo ucwords($row->user_phone); ?></td>
                                            <td><?php echo ucwords($row->tanggalacara); ?></td>

                                            <td><?php echo ucwords($row->barang_name); ?></td>
                                            <td><?php echo number_format($row->barang_price); ?></td>
                                            <td><?php echo ucwords($row->lamaacara); ?> Hari</td>
                                            <td><?php echo $row->jammulai .' / '.$row->jamselesai; ?></td>
                                            <td><?php echo number_format((int)$row->barang_price * (int)$row->lamaacara) ; ?></td>
                                            <td>
                                                <?php 
                                                if ($row->status == '0') {
                                                    echo 'Belum Bayar' ;
                                                } else if ($row->status == '1') {
                                                    echo 'Sudah Bayar' ;
                                                } else {
                                                    echo 'Proses' ; 
                                                } ?>
                                                    
                                            </td>
	                                        
	                                        <th>
	                                        	<button class="btn btn-sm btn-outline-default ripple btn-info" onclick="editdata(<?php echo $row->id_order;?>);">Edit</button>
	                                        	<button class="btn btn-sm btn-outline-default ripple btn-danger" onclick="deletedata(<?php echo $row->id_order;?>);">Delete</button>
	                                        </th>
	                                    </tr>
                                	<?php } ?>
                                	
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th>No</th>
                                        <th>Pembayaran</th>
                                        <th>Telp Vendor</th>
                                        <th>Tanggal Acara</th>
                                        <th>Nama Jasa</th>
                                        <th>Harga Jasa</th>
                                        <th>Lama Sewa</th>
                                        <th>Waktu</th>
                                        <th>Total Bayar</th>
                                        <th>Status Pesan</th>
                                        <th>Action</th>
                                	</tr>
                                </tfoot>
                            </table>
                        </div>
                        
                        <!-- /.widget-body -->
                    </div>
                    <!-- /.widget-bg -->
                </div>
            </div>
        </div>
	</main>

	<div class="modal modal-primary fade bs-modal-lg-primary" tabindex="-1" barang="dialog" id="modalcreate" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header text-inverse">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="myLargeModalLabel">Tambah Data</h5>
                </div>
                <div class="modal-body">
                    <?php echo form_open_multipart('c_barang/submitAddDataperUser'); ?>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label">Nama Jasa</label>
                            <div class="col-md-9">
                                <input class="form-control" type="text" name="barang_name">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label">Harga Jasa</label>
                            <div class="col-md-9">
                                <input class="form-control" type="number" name="barang_price">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label">Alamat Jasa</label>
                            <div class="col-md-9">
                                <input class="form-control" type="text" name="barang_region">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label">Disk Jasa</label>
                            <div class="col-md-9">
                                <input class="form-control" type="text" name="barang_desc">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label">Kategori</label>
                            <div class="col-md-9">
                                <select class="form-control" name="id_kategori">
                                	<?php foreach ($list_kategori as $key => $kat) { ?>
                                		<option value="<?php echo $kat->id; ?>"><?php echo $kat->kategori_name; ?></option>
                                	<?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-actions">
                            <div class="form-group row">
                                <div class="col-md-9 ml-md-auto btn-list">
                                    <input type="hidden" name="id_penjual" value="<?php echo $this->session->userdata['logged_in']['id'] ?>">
                                    <button class="btn btn-primary" type="Submit">Submit</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

    <div class="modal modal-primary fade bs-modal-lg-primary" tabindex="-1" barang="dialog" id="modaledit" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header text-inverse">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="myLargeModalLabel">Edit Data</h5>
                </div>
                <div class="modal-body">
                    <?php echo form_open_multipart('c_order/submitEditDatapembeli'); ?>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label">Nama Jasa</label>
                            <div class="col-md-9">
                                <input class="form-control" type="text" id="barang_name" readonly>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label">Harga Jasa</label>
                            <div class="col-md-9">
                                <input class="form-control" type="number" id="barang_price" readonly>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label">Alamat Jasa</label>
                            <div class="col-md-9">
                                <input class="form-control" type="text" id="barang_region" readonly>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label">Disk Jasa</label>
                            <div class="col-md-9">
                                <input class="form-control" type="text" id="barang_desc" readonly>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-md-3 col-form-label">Member</label>
                            <div class="col-md-9">
                                <input class="form-control" type="text" id="user_name" readonly>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-md-3 col-form-label">Upload Bukti Bayar</label>
                            <div class="col-md-9">
                                <input type="file" name="bukti_bayar" class="form-control">
                            </div>
                        </div>                       

                        <div class="form-actions">
                            <div class="form-group row">
                                <div class="col-md-9 ml-md-auto btn-list">
                                	<input type="hidden" name="id" id="id">
                                    <button class="btn btn-primary" type="Submit">Submit</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

	<?php include 'footer.php'; ?>
	<script type="text/javascript">
		var url="<?php echo site_url();?>";

        $(document ).ready(function() {
            $(".penjual").hide();
            $('#role option[value=1]').attr('selected','selected');

            var data_error = '<?php echo $msg; ?>';
            if (data_error != '') {
                $("#mesagge_error").text(data_error);           
                $("#status").modal('show');
                setInterval(function(){
                  window.location = url+"/c_f_home/";
                },1500) ;

                $("#main").css("display", "none");
                $("#main1").css("display", "none");
                
            } 
        });

		function editdata(id) {
	       $.ajax({
	        url: '<?php echo site_url('c_order/edit'); ?>/'+id,
	        type:'GET',
	        dataType: 'json',
	        success: function(data){
	          console.log(data);
	          $("#id").val(data['list_edit'][0]['id_order']);
	          $("#barang_name").val(data['list_edit'][0]['barang_name']);
	          $("#barang_price").val(data['list_edit'][0]['barang_price']);
	          $("#barang_region").val(data['list_edit'][0]['barang_region']);
	          $("#barang_desc").val(data['list_edit'][0]['barang_desc']);
	          $("#user_name").val(data['list_edit'][0]['user_name']);
	          $("#modaledit").modal('show');
	        }, 
	        error: function(){}
	      }); 
	    }

	    function deletedata(id){
	       var r=confirm("Apakah Data dengan ID "+id+" ingin di Hapus?")
	        if (r==true)
	          window.location = url+"/c_order/deleteperuser/"+id;
	        else
	          return false;
	    }
	</script>
</body>
</html>