<!DOCTYPE html>
<html>
<head>
	<title>Welcome</title>

	<?php include 'header.php'; ?>
</head>
<body class="header-dark sidebar-light sidebar-expand">
	<?php include 'navbar.php'; ?>
	<?php include 'sidebar.php'; ?>

	<main class="main-wrapper clearfix">
		<div class="row page-title clearfix">
		    <div class="page-title-left">
		        <h6 class="page-title-heading mr-0 mr-r-5">Order Menu</h6>
		    </div>
		</div>

		<div class="widget-list">
            <div class="row">
                <div class="col-md-12 widget-holder">
                    <div class="widget-bg">
                    	<div class="widget-body clearfix">
                            <table class="table table-striped table-responsive" data-toggle="datatables">
                                <thead>
                                    <tr>
                                        <th>Pemesan</th>
                                        <th>Telp Pemesan</th>
                                        <th>Alamat Pemesan</th>
                                        <th>Nama Jasa</th>
                                        <th>Tanggal Acara</th>
                                        <th>Lama Acara</th>                                        
                                        <th>Waktu</th>
                                        <th>Harga</th>
                                        <th>Total</th>
                                        <th>Status Pesan</th> 
                                        <th>Action</th> 
                                	</tr>
                                </thead>
                                <tbody>
                                    <?php $i = 1 ; ?>
                                    <?php //print_r($list); ?>
                                	<?php foreach ($list as $row ) { ?>
                                		<tr>
	                                        
	                                        <td><?php echo ucwords($row->user_name); ?></td>
                                            <td><?php echo ucwords($row->user_phone); ?></td>
                                            <td><?php echo ucwords($row->user_address); ?></td>                                            
                                            <td><?php echo ucwords($row->barang_name); ?></td>
                                            <td><?php echo ucwords($row->tanggalacara); ?></td>
                                            <td><?php echo ucwords($row->lamaacara); ?></td>
                                            <td><?php echo date('H:s', strtotime($row->jammulai)) .' / '.date('H:s', strtotime($row->jamselesai)); ?></td>

                                            <td><?php echo number_format($row->barang_price); ?></td>
                                            <td><?php echo number_format($row->barang_price * $row->lamaacara); ?></td>

                                            <td>
                                                <?php 
                                                if ($row->status == '0') {
                                                    echo 'Belum Bayar' ;
                                                } else if ($row->status == '1') {
                                                    echo 'Sudah Bayar' ;
                                                } else {
                                                    echo 'Proses' ; 
                                                } ?>
                                                    
                                            </td>
	                                        
	                                        <th>
	                                        	<button class="btn btn-sm btn-outline-default ripple btn-info" onclick="editdata(<?php echo $row->id_order;?>);">Edit</button>
	                                        	<button class="btn btn-sm btn-outline-default ripple btn-danger" onclick="deletedata(<?php echo $row->id_order;?>);">Delete</button>
	                                        </th>
	                                    </tr>
                                	<?php } ?>
                                	
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th>Pemesan</th>
                                        <th>Telp Pemesan</th>
                                        <th>Alamat Pemesan</th>
                                        <th>Nama Jasa</th>
                                        <th>Tanggal Acara</th>
                                        <th>Lama Acara</th>                                        
                                        <th>Waktu</th>
                                        <th>Harga</th>
                                        <th>Total</th>
                                        <th>Status Pesan</th> 
                                        <th>Action</th> 
                                	</tr>
                                </tfoot>
                            </table>
                        </div>
                        
                        <!-- /.widget-body -->
                    </div>
                    <!-- /.widget-bg -->
                </div>
            </div>
        </div>
	</main>

	<div class="modal modal-primary fade bs-modal-lg-primary" tabindex="-1" barang="dialog" id="modalcreate" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header text-inverse">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="myLargeModalLabel">Tambah Data</h5>
                </div>
                <div class="modal-body">
                    <?php echo form_open_multipart('c_barang/submitAddDataperUser'); ?>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label">Nama Jasa</label>
                            <div class="col-md-9">
                                <input class="form-control" type="text" name="barang_name">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label">Harga Jasa</label>
                            <div class="col-md-9">
                                <input class="form-control" type="number" name="barang_price">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label">Alamat Jasa</label>
                            <div class="col-md-9">
                                <input class="form-control" type="text" name="barang_region">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label">Disk Jasa</label>
                            <div class="col-md-9">
                                <input class="form-control" type="text" name="barang_desc">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label">Kategori</label>
                            <div class="col-md-9">
                                <select class="form-control" name="id_kategori">
                                	<?php foreach ($list_kategori as $key => $kat) { ?>
                                		<option value="<?php echo $kat->id; ?>"><?php echo $kat->kategori_name; ?></option>
                                	<?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-actions">
                            <div class="form-group row">
                                <div class="col-md-9 ml-md-auto btn-list">
                                    <input type="hidden" name="id_penjual" value="<?php echo $this->session->userdata['logged_in']['id'] ?>">
                                    <button class="btn btn-primary" type="Submit">Submit</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

    <div class="modal modal-primary fade bs-modal-lg-primary" tabindex="-1" barang="dialog" id="modaledit" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header text-inverse">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="myLargeModalLabel">Edit Data</h5>
                </div>
                <div class="modal-body">
                    <?php echo form_open_multipart('c_order/submitEditDataperuser'); ?>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label">Nama Jasa</label>
                            <div class="col-md-9">
                                <input class="form-control" type="text" id="barang_name" readonly>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label">Harga Jasa</label>
                            <div class="col-md-9">
                                <input class="form-control" type="number" id="barang_price" readonly>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label">Alamat Jasa</label>
                            <div class="col-md-9">
                                <input class="form-control" type="text" id="barang_region" readonly>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label">Disk Jasa</label>
                            <div class="col-md-9">
                                <input class="form-control" type="text" id="barang_desc" readonly>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-md-3 col-form-label">Member</label>
                            <div class="col-md-9">
                                <input class="form-control" type="text" id="user_name" readonly>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-md-3 col-form-label">Status</label>
                            <div class="col-md-9">
                                <a id="linkdownloadbukti">Download Bukti</a>
                                <select name="status" id="status" class="form-control">
                                    <option value="0">Belum Bayar</option>
                                    <option value="1">Sudah Bayar</option>
                                    <option value="2">Proses</option>
                                </select>
                            </div>
                        </div>                       

                        <div class="form-actions">
                            <div class="form-group row">
                                <div class="col-md-9 ml-md-auto btn-list">
                                	<input type="hidden" name="id" id="id">
                                    <button class="btn btn-primary" type="Submit">Submit</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

	<?php include 'footer.php'; ?>
	<script type="text/javascript">
		var url="<?php echo base_url();?>";

		function editdata(id) {
	       $.ajax({
	        url: '<?php echo site_url('c_order/edit'); ?>/'+id,
	        type:'GET',
	        dataType: 'json',
	        success: function(data){
	          console.log(data);
	          $("#id").val(data['list_edit'][0]['id_order']);
	          $("#barang_name").val(data['list_edit'][0]['barang_name']);
	          $("#barang_price").val(data['list_edit'][0]['barang_price']);
	          $("#barang_region").val(data['list_edit'][0]['barang_region']);
	          $("#barang_desc").val(data['list_edit'][0]['barang_desc']);
	          $("#user_name").val(data['list_edit'][0]['user_name']);
	          $("#status").val(data['list_edit'][0]['status']);

              $("#linkdownloadbukti").attr("href", url+"upload/buktibayar/"+data['list_edit'][0]['bukti_bayar']) ;

	          $("#modaledit").modal('show');
	        }, 
	        error: function(){}
	      }); 
	    }

	    function deletedata(id){
            var url="<?php echo site_url();?>";
	       var r=confirm("Apakah Data dengan ID "+id+" ingin di Hapus?")
	        if (r==true)
	          window.location = url+"/c_order/deleteperuser/"+id;
	        else
	          return false;
	    }
	</script>
</body>
</html>