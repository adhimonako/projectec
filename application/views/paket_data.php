<!DOCTYPE html>
<html>
<head>
	<title>Welcome</title>

	<?php include 'header.php'; ?>
</head>
<body class="header-dark sidebar-light sidebar-expand">
	<?php include 'navbar.php'; ?>
	<?php include 'sidebar.php'; ?>

	<main class="main-wrapper clearfix">
		<div class="row page-title clearfix">
		    <div class="page-title-left">
		        <h6 class="page-title-heading mr-0 mr-r-5">Paket Menu</h6>
		    </div>
		</div>

		<div class="widget-list">
            <div class="row">
                <div class="col-md-12 widget-holder">
                    <div class="widget-bg">
                    	<div class="widget-body clearfix">
                    		<div class=" col-md-2">
                    			<button class="btn btn-block btn-success ripple" data-toggle="modal" data-target="#modalcreate">Tambah</button>
                    		</div>
                            <table class="table table-striped table-responsive" data-toggle="datatables">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Nama</th>
                                        <th>Harga</th>
                                        <th>Disk</th>
                                        <th>Action</th>
                                	</tr>
                                </thead>
                                <tbody>
                                	<?php $i = 1 ; ?>
                                	<?php foreach ($list as $row ) { ?>
                                		<tr>
	                                        <td><?php echo $i++; ?></td>
	                                        <td><?php echo ucwords($row->nama_paket); ?></td>
	                                        <td><?php echo number_format($row->harga_paket); ?></td>
                                            <td><?php echo ucwords($row->deskripsi_paket); ?></td>
	                                        <th>
	                                        	<button class="btn btn-sm btn-outline-default ripple btn-info" onclick="editdata(<?php echo $row->id;?>);">Edit</button>
	                                        	<button class="btn btn-sm btn-outline-default ripple btn-danger" onclick="deletedata(<?php echo $row->id;?>);">Delete</button>
	                                        </th>
	                                    </tr>
                                	<?php } ?>
                                	
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th>No</th>
                                        <th>Nama</th>
                                        <th>Harga</th>
                                        <th>Disk</th>
                                        <th>Action</th>
                                	</tr>
                                </tfoot>
                            </table>
                        </div>
                        
                        <!-- /.widget-body -->
                    </div>
                    <!-- /.widget-bg -->
                </div>
            </div>
        </div>
	</main>

	<div class="modal modal-primary fade bs-modal-lg-primary" tabindex="-1" barang="dialog" id="modalcreate" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header text-inverse">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="myLargeModalLabel">Tambah Data</h5>
                </div>
                <div class="modal-body">
                    <?php echo form_open_multipart('c_paket/submitadddata'); ?>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label">Nama Paket</label>
                            <div class="col-md-9">
                                <input class="form-control" type="text" name="nama_paket">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label">Harga Paket</label>
                            <div class="col-md-9">
                                <input class="form-control" type="number" name="harga_paket">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label">Paket Tradisi</label>
                            <div class="col-md-9">
                                <input class="form-control" type="text" name="tradisi_paket">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label">Disk Paket</label>
                            <div class="col-md-9">
                                <input class="form-control" type="text" name="deskripsi_paket">
                            </div>
                        </div>

                        <?php if ($this->session->userdata['logged_in']['user_roleid'] == '1') { ?>
                            <div class="form-group row">
                                <label class="col-md-3 col-form-label">Penjual</label>
                                <div class="col-md-9">
                                    <select class="form-control" name="id_penjual">
                                        <?php foreach ($list_user as $key => $usr) { ?>
                                            <option value="<?php echo $usr->id; ?>"><?php echo $usr->user_name; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>

                             <div class="form-group row">
                                <label class="col-md-3 col-form-label">Kategori</label>
                                <div class="col-md-9">
                                    <select class="form-control" name="id_kategori">
                                        <?php foreach ($list_kategori as $key => $kat) { ?>
                                            <option value="<?php echo $kat->id; ?>"><?php echo $kat->kategori_name; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                        <?php } else if ($this->session->userdata['logged_in']['user_roleid'] == '2') { ?>
                            <input type="hidden" name="id_penjual" value="<?php echo $this->session->userdata['logged_in']['id']; ?>">
                        <?php } ?>
                        

                        <div class="form-group row">
                            <label class="col-md-3 col-form-label">Barang</label>
                            <div class="col-md-9">
                               <?php foreach ($barang as $key => $barang_y) { ?>
                                    <input type="checkbox" name="id_barang[]" value="<?php echo $barang_y->id; ?>"> <?php echo $barang_y->barang_name ?> <br>
                                <?php } ?>
                            </div>
                        </div>

                        <div class="form-actions">
                            <div class="form-group row">
                                <div class="col-md-9 ml-md-auto btn-list">
                                    <button class="btn btn-primary" type="Submit">Submit</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

    <div class="modal modal-primary fade bs-modal-lg-primary" tabindex="-1" barang="dialog" id="modaledit" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header text-inverse">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="myLargeModalLabel">Edit Data</h5>
                </div>
                <div class="modal-body">
                    <?php echo form_open_multipart('c_paket/submiteditdata'); ?>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label">Nama Paket</label>
                            <div class="col-md-9">
                                <input class="form-control" type="text" name="nama_paket" id="nama_paket">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label">Harga Paket</label>
                            <div class="col-md-9">
                                <input class="form-control" type="number" name="harga_paket" id="harga_paket">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label">Paket Tradisi</label>
                            <div class="col-md-9">
                                <input class="form-control" type="text" name="tradisi_paket" id="tradisi_paket">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label">Disk Paket</label>
                            <div class="col-md-9">
                                <input class="form-control" type="text" name="deskripsi_paket" id="deskripsi_paket">
                            </div>
                        </div>
                        
                        <?php if ($this->session->userdata['logged_in']['user_roleid'] == '1') { ?>

                        <div class="form-group row">
                            <label class="col-md-3 col-form-label">Kategori</label>
                            <div class="col-md-9">
                                <select class="form-control" name="id_kategori" id="id_kategori">
                                	<?php foreach ($list_kategori as $key => $kat) { ?>
                                		<option value="<?php echo $kat->id; ?>"><?php echo $kat->kategori_name; ?></option>
                                	<?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label">Vendor</label>
                            <div class="col-md-9">
                                <select class="form-control" name="id_penjual" id="id_penjual">
                                	<?php foreach ($list_user as $key => $usr) { ?>
                                		<option value="<?php echo $usr->id; ?>"><?php echo $usr->user_name; ?></option>
                                	<?php } ?>
                                </select>
                            </div>
                        </div>
                        <?php } ?>

                         <div class="form-group row">
                            <label class="col-md-3 col-form-label">Photo Paket</label>
                            <div class="col-md-9">
                                <input type="file" name="photo_paket">
                            </div>
                        </div>

                         <div class="form-group row">
                            <label class="col-md-3 col-form-label">Photo Paket</label>
                            <div class="col-md-9">
                                <input type="file" name="photo_paket1">
                            </div>
                        </div>

                         <div class="form-group row">
                            <label class="col-md-3 col-form-label">Photo Paket</label>
                            <div class="col-md-9">
                                <input type="file" name="photo_paket2">
                            </div>
                        </div>

                         <div class="form-group row">
                            <label class="col-md-3 col-form-label">Photo Paket</label>
                            <div class="col-md-9">
                                <input type="file" name="photo_paket3">
                            </div>
                        </div>

                         <div class="form-group row">
                            <label class="col-md-3 col-form-label">Photo Paket</label>
                            <div class="col-md-9">
                                <input type="file" name="photo_paket4">
                            </div>
                        </div>


                        <div class="form-group row">
                            <label class="col-md-3 col-form-label">Jasa</label>
                            <div class="col-md-9">
                                <?php foreach ($barang as $key => $barangx) { ?>
                                    <input type="checkbox" name="id_barang[]" value="<?php echo $barangx->id; ?>" checked> <?php echo $barangx->barang_name ?> <br>
                                <?php } ?>
                            </div>
                        </div>

                        <div class="form-actions">
                            <div class="form-group row">
                                <div class="col-md-9 ml-md-auto btn-list">
                                	<input type="hidden" name="id" id="id">
                                    <input type="hidden" name="id_penjual" id="id_penjual">
                                    <button class="btn btn-primary" type="Submit">Submit</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

	<?php include 'footer.php'; ?>
	<script type="text/javascript">
		var url="<?php echo site_url();?>";

		function editdata(id) {
	       $.ajax({
	        url: '<?php echo site_url('c_paket/edit'); ?>/'+id,
	        type:'GET',
	        dataType: 'json',
	        success: function(data){
	          console.log(data);
	          $("#id").val(data['list_edit'][0]['id']);
	          $("#nama_paket").val(data['list_edit'][0]['nama_paket']);
	          $("#deskripsi_paket").val(data['list_edit'][0]['deskripsi_paket']);
	          $("#tradisi_paket").val(data['list_edit'][0]['tradisi_paket']);
	          $("#harga_paket").val(data['list_edit'][0]['harga_paket']);
	          $("#id_penjual").val(data['list_edit'][0]['id_penjual']);

	          $("#modaledit").modal('show');
	        }, 
	        error: function(){}
	      }); 
	    }

	    function deletedata(id){
	       var r=confirm("Apakah Data dengan ID "+id+" ingin di Hapus?")
	        if (r==true)
	          window.location = url+"/c_paket/delete/"+id;
	        else
	          return false;
	    }
	</script>
</body>
</html>