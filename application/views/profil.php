
<!DOCTYPE html>
<html>
<head>
	<title>Welcome</title>

	<?php include 'header.php'; ?>

      <style type="text/css">
        .modal-backdrop {
            z-index: -1;
        }

        .thumb-sm img {
            max-height: 999px!important;
        }

    </style>


</head>
<body class="header-dark sidebar-light sidebar-expand">
	<?php include 'navbar.php'; ?>
	<?php include 'sidebar.php'; ?>

    <div class="modal fade" id="status" style="z-index: 1001;">
      <div class="modal-dialog">
        <div class="modal-content">
            
                <!-- Modal Header -->
                <div class="modal-header">
                    <h5 class="modal-title">Pemberitahuan</h5>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>

                <!-- Modal body -->
                <div class="modal-body">
                    <h4 id="mesagge_error">Pendaftaran Berhasil</h4>
                    <b><h3 id="jumlahpesanan" style="color: green;">Pendaftaran Berhasil</h3></b>
                </div>
             
        </div>
      </div>
    </div>

	<main class="main-wrapper clearfix">
		<div class="row page-title clearfix">
		    <div class="page-title-left">
		        <h6 class="page-title-heading mr-0 mr-r-5">Profil</h6>
		    </div>
		</div>

		<div class="widget-list">
            <div class="row">
                <div class="col-md-12 widget-holder">

                    <div class="widget-bg">
                        <div class="widget-body clearfix">
                            
                            <div class=" col-md-2">
                                <button class="btn btn-block btn-success ripple" onclick="editdata(<?php echo $list_edit[0]->id;?>)">Update</button>
                            </div>
                            
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="row">
                                        <div class="col-md-6"><b>Nama</b></div>
                                        <div class="col-md-6"><?php echo $list_edit[0]->user_name; ?></div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6"><b>Alamat</b></div>
                                        <div class="col-md-6"><?php echo $list_edit[0]->user_address; ?></div>
                                    </div>
                                </div>

                                 <div class="col-md-6">
                                    <div class="row">
                                        <div class="col-md-6"><b>Email</b></div>
                                        <div class="col-md-6"><?php echo $list_edit[0]->user_mail; ?></div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6"><b>Jenis</b></div>
                                        <div class="col-md-6">
                                            <?php 
                                                if ($list_edit[0]->user_roleid == '2') {
                                                     echo 'Penjual';
                                                  }  else if ($list_edit[0]->user_roleid == '3') {
                                                      echo 'Pembeli' ;
                                                  } else {
                                                     echo 'Admin';
                                                  }
                                            ?>
                                            

                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="row">
                                        <div class="col-md-6"><b>No Telp</b></div>
                                        <div class="col-md-6"><?php echo $list_edit[0]->user_phone; ?></div>
                                    </div>
                                </div>
                            </div>    

                            <?php if ($list_edit[0]->user_roleid == '1') { ?>
                         
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="row">
                                            <div class="col-md-6"><b>No Rek</b></div>
                                            <div class="col-md-6"><?php echo $list_edit[0]->user_banknumber; ?></div>
                                        </div>
                                    </div>
                                </div>    

                            
                            <?php } ?>                       
                    		
                        </div>    

                     

                        <?php if ($list_edit[0]->user_roleid == '2') { ?>
                            <div class="widget-body">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="row">
                                            <div class="col-md-6"><b>Status Langganan</b></div>
                                             <div class="col-md-6"><?php echo $list_edit[0]->user_status_langganan != '1'? "Belum Aktif" : 'Sudah Aktif'; ?></div>
                                        </div>

                                        <?php if ($list_edit[0]->user_status_langganan != '1') { ?>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <h5>Status Anda Belum Aktif Silahkan melakukan Pembayaran ke</h5>
                                                    <h5><b><?php echo $list_admin->user_banknumber .' - '. $list_admin->user_name;?></b></h5>
                                                </div>
                                            </div>
                                        <?php }  else { ?>

                                        <div class="row">
                                            <div class="col-md-6"><b>Jenis Langganan</b></div>
                                             <div class="col-md-6"><?php echo ucwords($list_edit[0]->user_jenis_langganan); ?></div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-6"><b>Nama Bisnis</b></div>
                                            <div class="col-md-6"><?php echo $list_edit[0]->user_bussiness; ?></div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6"><b>No Rekening</b></div>
                                            <div class="col-md-6"><?php echo $list_edit[0]->user_banknumber; ?></div>
                                        </div>

                                        <?php } ?>
                                    </div>
                                </div>
                            </div>   
                        <?php } ?>
                                         
                        <!-- /.widget-body -->
                    </div>
                    <!-- /.widget-bg -->
                </div>
            </div>
        </div>
	</main>



    <div class="modal modal-primary fade bs-modal-lg-primary" tabindex="-1" user="dialog" id="modaledit" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header text-inverse">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="myLargeModalLabel">Edit Data</h5>
                </div>
                <div class="modal-body">
                    <?php echo form_open_multipart('c_user/submitEditDataProfil'); ?>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label">Nama User</label>
                            <div class="col-md-9">
                                <input class="form-control" type="text" name="user_name" id="user_name">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label">Phone User</label>
                            <div class="col-md-9">
                                <input class="form-control" type="number" name="user_phone" id="user_phone">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label">Alamat User</label>
                            <div class="col-md-9">
                                <input class="form-control" type="text" name="user_address" id="user_address">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label">Email User</label>
                            <div class="col-md-9">
                                <input class="form-control" type="email" name="user_mail" id="user_mail">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label">Password User</label>
                            <div class="col-md-9">
                                <input class="form-control" type="password" name="user_password" id="user_password">
                            </div>
                        </div>
                        
                        <?php if ($list_edit[0]->user_roleid == '1') { ?>
                              <div class="form-group row">
                                <label class="col-md-3 col-form-label">Account Bank Number</label>
                                <div class="col-md-9">
                                    <input class="form-control" type="text" name="user_banknumber" id="user_banknumber">
                                </div>
                            </div>
                        <?php } ?>


                        <?php if ($list_edit[0]->user_roleid == '2') { ?>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label">Nama Bisnis</label>
                            <div class="col-md-9">
                                <input class="form-control" type="text" name="user_bussiness" id="user_bussiness">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-md-3 col-form-label">Account Bank Number</label>
                            <div class="col-md-9">
                                <input class="form-control" type="text" name="user_banknumber" id="user_banknumber">
                            </div>
                        </div>

                       <div class="form-group row">
                            <label class="col-md-3 col-form-label">Berlangganan</label>
                            <div class="col-md-9">
                                <select name="user_jenis_langganan" class="form-control">
                                    <option value="1 Bulan - Rp 50.000">1 Bulan - Rp 50.000</option>
                                    <option value="2 Bulan - Rp 100.000">2 Bulan - Rp 100.000</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-md-3 col-form-label">Upload Bukti Berlangganan</label>
                            <div class="col-md-9">
                                <input type="file" name="user_photo_langganan">
                            </div>
                        </div>
                        <?php } ?>

                        
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label">Photo Profil</label>
                            <div class="col-md-9">
                                <input type="file" name="user_photo">
                            </div>
                        </div>

                        

                        <div class="form-actions">
                            <div class="form-group row">
                                <div class="col-md-9 ml-md-auto btn-list">
                                	<input type="hidden" name="id" id="id">
                                    <button class="btn btn-primary" type="Submit">Submit</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

	<?php include 'footer.php'; ?>
	<script type="text/javascript">
        
		var url="<?php echo site_url();?>";

        $(document ).ready(function() {
         var data_error = '<?php echo $msg; ?>';
         var jml = '<?php echo $psanana; ?>';
         console.log(data_error);
            if (data_error != '') {
                $("#mesagge_error").text(data_error);     
                $("#jumlahpesanan").text(jml+' Pesanan Baru');           
                $("#status").modal('show');
               
            } 
        });


		function editdata(id) {
	       $.ajax({
	        url: '<?php echo site_url('c_user/edit'); ?>/'+id,
	        type:'GET',
	        dataType: 'json',
	        success: function(data){
	          // console.log(data);
	          $("#id").val(data['list_edit'][0]['id']);
	          $("#user_name").val(data['list_edit'][0]['user_name']);
	          $("#user_phone").val(data['list_edit'][0]['user_phone']);
	          $("#user_address").val(data['list_edit'][0]['user_address']);
	          $("#user_mail").val(data['list_edit'][0]['user_mail']);
	          $("#user_password").val(data['list_edit'][0]['user_password']);
	          $("#user_roleid").val(data['list_edit'][0]['user_roleid']);
	          $("#user_bussiness").val(data['list_edit'][0]['user_bussiness']);
	          $("#user_banknumber").val(data['list_edit'][0]['user_banknumber']);
	          $("#modaledit").modal('show');
	        }, 
	        error: function(){}
	      }); 
	    }

	    function deletedata(id){
	       var r=confirm("Apakah Data dengan ID "+id+" ingin di Hapus?")
	        if (r==true)
	          window.location = url+"/c_user/delete/"+id;
	        else
	          return false;
	    }
	</script>
</body>
</html>