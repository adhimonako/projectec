<div class="content-wrapper">
    <!-- SIDEBAR -->
    <aside class="site-sidebar scrollbar-enabled" data-suppress-scroll-x="true">
        <!-- User Details -->
        <div class="side-user">
           

            <div class="col-sm-12 text-center p-0 clearfix">
                <div class="d-inline-block pos-relative mr-b-10">
                    <figure class="thumb-sm mr-b-0 user--online" style="height: 60px;">
                        <img src="<?php echo base_url().'/upload/userphoto/'.$this->session->userdata['logged_in']['user_photo']?>" class="rounded-circle" alt="" height="60" style="height: 60px;">
                        
                    </figure><a href="#" class="text-muted side-user-link"><i class="feather feather-settings list-icon"></i></a>
                </div>
                <div class="lh-14 mr-t-5">
                    <a href="<?php echo site_url('c_user/profil/'.$this->session->userdata['logged_in']['id']);?>" class="hide-menu mt-3 mb-0 side-user-heading fw-500"><?php echo $this->session->userdata['logged_in']['user_name'] ?></a>
                    <br><small class="hide-menu">&nbsp;</small>
                </div>
            </div>
                <!-- /.col-sm-12 -->
            
            <!-- /.col-sm-12 -->
        </div>
        <!-- /.side-user -->
        <!-- Sidebar Menu -->
        <nav class="sidebar-nav">
            <ul class="nav in side-menu">
                

                <?php if ($this->session->userdata['logged_in']['user_roleid'] == '2') { ?>
                    <li class="current-page menu-item-has-children"><a href="javascript:void(0);"><i class="list-icon feather feather-command"></i> <span class="hide-menu">Vendor</span></a>
                        <ul class="list-unstyled sub-menu">
                            <li><a href="<?php echo site_url('c_user/profil/'.$this->session->userdata['logged_in']['id']);?>">Profil</a></li>

                            <?php if ($this->session->userdata['logged_in']['user_status_langganan'] == '1') { ?>
                                <li><a href="<?php echo site_url('c_barang/barangperuser');?>">Jasa</a></li>
                                <li><a href="<?php echo site_url('c_paket');?>">Paket</a></li>
                                <li><a href="<?php echo site_url('c_order/pesananperuser');?>">Pesanan</a></li>    
                                <li><a href="<?php echo site_url('c_order/pesananperuserpaket');?>">Pesanan Paket</a></li>    
                            <?php } ?>
                            
                        </ul>
                    </li>
                <?php } else if ($this->session->userdata['logged_in']['user_roleid'] == '3') { ;?>
                     

                     <li class="current-page menu-item-has-children"><a href="javascript:void(0);"><i class="list-icon feather feather-command"></i> <span class="hide-menu">Member</span></a>
                        <ul class="list-unstyled sub-menu">
                            <li><a href="<?php echo site_url('c_user/profil/'.$this->session->userdata['logged_in']['id']);?>">Profil</a></li>
                            <li><a href="<?php echo site_url('c_order/pesananpembeli');?>">Pesanan</a></li>
                            <li><a href="<?php echo site_url('c_order/pesananpembelipaket');?>">Pesanan Paket</a></li>
                        </ul>
                    </li>
                <?php } else { ?>

                <li class="current-page menu-item-has-children"><a href="javascript:void(0);"><i class="list-icon feather feather-command"></i> <span class="hide-menu">Dashboard</span></a>
                    <ul class="list-unstyled sub-menu">
                        <li><a href="<?php echo site_url('c_user/profil/'.$this->session->userdata['logged_in']['id']);?>">Profil</a></li>
                        <li><a href="<?php echo site_url('c_role');?>">Role</a></li>
                        <li><a href="<?php echo site_url('c_user');?>">User</a></li>
                        <li><a href="<?php echo site_url('c_kategori');?>">Kategori</a></li>
                        <li><a href="<?php echo site_url('c_barang');?>">Jasa</a></li>
                        <!--
                        <li><a href="index-2.html">Default</a></li>
                        <li><a href="http://dharansh.in/bonvuehtml/real-estate/index.html">Real Estate</a></li>
                        <li><a href="http://dharansh.in/bonvuehtml/job-board/index.html">Jobs Board</a></li>
                         -->
                    </ul>
                </li>

                <?php } ?>

                <li class="current-page menu-item-has-children"><a href="javascript:void(0);"><i class="list-icon feather feather-command"></i> <span class="hide-menu">More</span></a>
                    <ul class="list-unstyled sub-menu">
                        <li><a href="<?php echo site_url('c_user/logout');?>">Log Out</a></li>
                        <!--
                        <li><a href="index-2.html">Default</a></li>
                        <li><a href="http://dharansh.in/bonvuehtml/real-estate/index.html">Real Estate</a></li>
                        <li><a href="http://dharansh.in/bonvuehtml/job-board/index.html">Jobs Board</a></li>
                         -->
                    </ul>
                </li>

                <!--
                <li class="menu-item-has-children active"><a href="javascript:void(0);"><i class="list-icon feather feather-briefcase"></i> <span class="hide-menu">Apps</span></a>
                    <ul class="list-unstyled sub-menu">
                        <li><a href="app-calender.html">Calender</a>
                        </li>
                        <li><a href="app-chat.html">Chat</a>
                        </li>
                        <li class="menu-item-has-children"><a href="javascript:void(0);">Inbox</a>
                            <ul class="list-unstyled sub-menu">
                                <li><a href="app-inbox.html">Inbox</a>
                                </li>
                                <li><a href="app-inbox-compose.html">Compose mail</a>
                                </li>
                            </ul>
                        </li>
                        <li><a href="app-contacts.html">Contacts</a>
                        </li>
                        <li><a href="app-products.html">Products</a>
                        </li>
                    </ul>
                </li>
                <li class="menu-item-has-children"><a href="javascript:void(0);"><i class="list-icon feather feather-user"></i> <span class="hide-menu">Profile Pages</span></a>
                    <ul class="list-unstyled sub-menu">
                        <li><a href="page-profile.html">Profile</a>
                        </li>
                        <li><a href="page-login.html">Login Page</a>
                        </li>
                        <li><a href="page-login2.html">Login Page 2</a>
                        </li>
                        <li><a href="page-register.html">Sign Up</a>
                        </li>
                        <li><a href="page-register2.html">Sign Up 2</a>
                        </li>
                        <li><a href="page-register-3-step.html">3 Step Sign Up</a>
                        </li>
                        <li><a href="page-forgot-pwd.html">Forgot Password</a>
                        </li>
                        <li><a href="page-email-confirm.html">Confirm Email</a>
                        </li>
                        <li><a href="page-lock-screen.html">Lock Screen</a>
                        </li>
                        <li><a href="page-timeline.html">Timeline</a>
                        </li>
                        <li class="menu-item-has-children"><a href="javascript:void(0);">Error Pages</a>
                            <ul class="list-unstyled sub-menu">
                                <li><a href="page-error-403.html">Error 403</a>
                                </li>
                                <li><a href="page-error-404.html">Error 404</a>
                                </li>
                                <li><a href="page-error-500.html">Error 500</a>
                                </li>
                                <li><a href="page-error-503.html">Error 503</a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </li>
                <li class="menu-item-has-children"><a href="javascript:void(0);"><i class="list-icon feather feather-feather"></i> <span class="hide-menu">UI Elements</span></a>
                    <ul class="list-unstyled sub-menu">
                        <li><a href="ui-typography.html">Typography</a>
                        </li>
                        <li><a href="ui-buttons.html">Buttons</a>
                        </li>
                        <li><a href="ui-cards.html">Cards</a>
                        </li>
                        <li><a href="ui-tabs.html">Tabs</a>
                        </li>
                        <li><a href="ui-accordions.html">Accordions</a>
                        </li>
                        <li><a href="ui-modals.html">Modals</a>
                        </li>
                        <li><a href="ui-icon-boxes.html">Icon Boxes</a>
                        </li>
                        <li><a href="ui-lists.html">Lists &amp; Media Object</a>
                        </li>
                        <li><a href="ui-grid.html">Grid</a>
                        </li>
                        <li><a href="ui-progress.html">Progress Bars</a>
                        </li>
                        <li><a href="ui-notifications.html">Notifications &amp; Alerts</a>
                        </li>
                        <li><a href="ui-pagination.html">Pagination</a>
                        </li>
                        <li><a href="ui-media.html">Media</a>
                        </li>
                        <li><a href="ui-carousel.html">Carousel</a>
                        </li>
                        <li><a href="ui-bootstrap.html">Bootstrap Elements</a>
                        </li>
                    </ul>
                </li>
                <li class="menu-item-has-children"><a href="javascript:void(0);"><i class="list-icon feather feather-layout"></i> <span class="hide-menu">Forms</span></a>
                    <ul class="list-unstyled sub-menu">
                        <li><a href="form-elements.html">Elements</a>
                        </li>
                        <li><a href="form-material.html">Material Design</a>
                        </li>
                        <li><a href="form-validation.html">Form Validation</a>
                        </li>
                        <li><a href="form-dropzone.html">File Upload</a>
                        </li>
                        <li><a href="form-pickers.html">Picker</a>
                        </li>
                        <li><a href="form-select.html">Select and Multiselect</a>
                        </li>
                        <li><a href="form-tags-categories.html">Tags and Categories</a>
                        </li>
                        <li><a href="form-addons.html">Addons</a>
                        </li>
                        <li><a href="form-editors.html">Editors</a>
                        </li>
                    </ul>
                </li>
                <li class="menu-item-has-children"><a href="javascript:void(0);"><i class="list-icon feather feather-folder"></i> <span class="hide-menu">Sample Pages</span></a>
                    <ul class="list-unstyled sub-menu">
                        <li><a href="page-blank.html">Blank Page</a>
                        </li>
                        <li class="menu-item-has-children"><a href="javascript:void(0);">Email Templates</a>
                            <ul class="list-unstyled sub-menu">
                                <li><a href="email-templates/basic.html">Basic</a>
                                </li>
                                <li><a href="email-templates/billing.html">Billing</a>
                                </li>
                                <li><a href="email-templates/friend-request.html">Friend Request</a>
                                </li>
                            </ul>
                        </li>
                        <li class="menu-item-has-children"><a href="javascript:void(0);">Maps</a>
                            <ul class="list-unstyled sub-menu">
                                <li><a href="maps-google.html">Google Maps</a>
                                </li>
                                <li><a href="maps-vector.html">Vector Maps</a>
                                </li>
                            </ul>
                        </li>
                        <li><a href="page-lightbox.html">Lightbox Popup</a>
                        </li>
                        <li><a href="page-sitemap.html">Sitemap</a>
                        </li>
                        <li><a href="page-search-results.html">Search Results</a>
                        </li>
                        <li><a href="page-custom-scroll.html">Custom Scroll</a>
                        </li>
                        <li><a href="page-utility-classes.html">Utility Classes</a>
                        </li>
                        <li><a href="page-animations.html">Animations</a>
                        </li>
                        <li><a href="page-faq.html">FAQ</a>
                        </li>
                        <li><a href="page-pricing-table.html">Pricing</a>
                        </li>
                        <li><a href="page-invoice.html">Invoice</a>
                        </li>
                    </ul>
                </li>
                <li class="menu-item-has-children"><a href="javascript:void(0);"><i class="list-icon feather feather-clipboard"></i> <span class="hide-menu">Tables</span></a>
                    <ul class="list-unstyled sub-menu">
                        <li><a href="tables-basic.html">Basic Tables</a>
                        </li>
                        <li><a href="tables-data-table.html">Data Table</a>
                        </li>
                        <li><a href="tables-bootstrap.html">Bootstrap Tables</a>
                        </li>
                        <li><a href="tables-responsive.html">Responsive Tables</a>
                        </li>
                        <li><a href="tables-editable.html">Editable Tables</a>
                        </li>
                    </ul>
                </li>
                <li class="menu-item-has-children"><a href="javascript:void(0);"><i class="list-icon feather feather-pie-chart"></i> <span class="hide-menu">Charts</span></a>
                    <ul class="list-unstyled sub-menu">
                        <li><a href="charts-flot.html">Flot Charts</a>
                        </li>
                        <li><a href="charts-morris.html">Morris Charts</a>
                        </li>
                        <li><a href="charts-js.html">Chart-js</a>
                        </li>
                        <li><a href="charts-sparkline.html">Sparkline Charts</a>
                        </li>
                        <li><a href="charts-knob.html">Knob Charts</a>
                        </li>
                    </ul>
                </li>
                <li class="menu-item-has-children"><a href="javascript:void(0);"><i class="list-icon feather feather-heart"></i> <span class="hide-menu">Icons</span></a>
                    <ul class="list-unstyled sub-menu">
                        <li><a href="icons-material-design.html">Material Design</a>
                        </li>
                        <li><a href="icons-font-awesome.html">Font Awesome</a>
                        </li>
                        <li><a href="icons-mono-social.html">Social Icons</a>
                        </li>
                        <li><a href="icons-weather.html">Weather Icons</a>
                        </li>
                        <li><a href="icons-linea.html">Linea Icons</a>
                        </li>
                        <li><a href="icons-feather.html">Feather Icons</a>
                        </li>
                    </ul>
                </li>
                 -->
            </ul>
            <!-- /.side-menu -->
        </nav>
        <!-- /.sidebar-nav -->
    </aside>
    
</div>