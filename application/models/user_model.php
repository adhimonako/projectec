<?php
class User_Model extends CI_Model {

    public function __construct() {
		parent::__construct();

        $this->load->database(); 

        ## declate table name here
        $this->table_name = 'ms_user' ;
    }

    function inputData() {
        $a_input = array();
       
        foreach ($_POST as $key => $value) {
            $a_input[$key] = strtolower($value);
        }

        $a_input['user_password'] = $a_input['user_password'];

        $this->db->insert($this->table_name, $a_input);
        return  $this->db->_error_message();
    
    }

    function editData($id) {
        ## unset supaya id tidak terambil
        unset($_POST['id']);

        foreach ($_POST as $key => $value) {
            $a_input[$key] = strtolower($value);
        }

        if (!empty($_FILES['user_photo_langganan'])) {
            $a_input['user_photo_langganan'] = str_replace(" ","_",$_FILES['user_photo_langganan']['name']);
        }

        if (!empty($_FILES['user_photo'])) {
            $a_input['user_photo'] = str_replace("","_",$_FILES['user_photo']['name']);
        }

        $this->db->where('id', $id);
        $this->db->update($this->table_name, $a_input);
    }

	public function deleteData($id) {
		$this->db->where('id', $id);
        $this->db->delete($this->table_name);

        return $id;
	}

    function getAllData() {
        $query = $this->db->get($this->table_name);

        return $query->result();
	}

    function getAllDataByID($id) {
        $this->db->where(array('id' => $id));
        
        $query = $this->db->get($this->table_name);
        
        return $query->result();
    }

    function getAllDataByAdmin() {
        $this->db->where(array('user_roleid' => '1'));
        
        $query = $this->db->get($this->table_name);
        
        return $query->row();
    }

    function getAllDataPenjual() {
        $this->db->where(array('user_roleid' => '2'));
        $query = $this->db->get($this->table_name);

        return $query->result();
    }

    function getUserLogin($parameter) {
        $this->db->where("user_roleid !=",'1');
        $this->db->where(array('user_name' => $parameter[0],'user_password' => $parameter[1]));
        $query = $this->db->get($this->table_name);

        return $query->row();
    }

    function getUserLoginAdmin($parameter) {
        $this->db->where("user_roleid =",'1');
        $this->db->where(array('user_name' => $parameter[0],'user_password' => $parameter[1]));
        $query = $this->db->get($this->table_name);

        return $query->row();
    }
}