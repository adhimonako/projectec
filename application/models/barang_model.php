<?php
class Barang_Model extends CI_Model {

    public function __construct() {
		parent::__construct();

        ## declate table name here
        $this->table_name = 'ms_barang' ;
    }

    function inputData() {
        $a_input = array();
       
        foreach ($_POST as $key => $value) {
            $a_input[$key] = strtolower($value);
        }

        $this->db->insert($this->table_name, $a_input);
    }

    function editData($id) {
        ## unset supaya id tidak terambil
        unset($_POST['id']);

        /*
        foreach ($_POST as $key => $value) {
            $a_input[$key] = strtolower($value);
        }
        */

        $a_input['barang_name'] = strtolower($_POST['barang_name']);
        $a_input['barang_price'] = strtolower($_POST['barang_price']);
        $a_input['barang_region'] = strtolower($_POST['barang_region']);
        $a_input['barang_desc'] = strtolower($_POST['barang_desc']);
        $a_input['id_kategori'] = strtolower($_POST['id_kategori']);

        if (!empty($_FILES['photo_barang'])) {
            $a_input['photo_barang'] = $_FILES['photo_barang']['name'];
        }

        if (empty($a_input['photo_barang'])) {
            unset($a_input['photo_barang']);
        }

        for ($i=1; $i < 5; $i++) { 
            if (!empty($_FILES['photo_barang'.$i])) {
                $a_input['photo_barang'.$i] = $_FILES['photo_barang'.$i]['name'];
            }

            if (empty($a_input['photo_barang'.$i])) {
                unset($a_input['photo_barang'.$i]);
            }
        }

        $this->db->where('id', $id);
        $this->db->update($this->table_name, $a_input);
    }

	public function deleteData($id) {
		$this->db->where('id', $id);
        $this->db->delete($this->table_name);

        return $id;
	}

    function getAllData() {
        $query = $this->db->get($this->table_name);

        return $query->result();
	}

    function getAllDatabysearch($word) {
        
        $where = "barang_name like '%$word%' OR barang_region like '%$word%'";
        $this->db->where($where);
        $query = $this->db->get($this->table_name);

        return $query->result();
    }


    function getAllDataByID($id) {
        $this->db->where(array('id' => $id));
        
        $query = $this->db->get($this->table_name);
        
        return $query->result();
    }

    function getAllDataperUser($id) {
        $this->db->where(array('id_penjual' => $id));
        $query = $this->db->get($this->table_name);

        return $query->result();
    }

    function getAllDataByIDKategori($id) {
        $this->db->where(array('id_kategori' => $id));
        
        $query = $this->db->get($this->table_name);
        
        return $query->result();
    }

    function getAllDataByIDKategorisearch($id,$word=null) {
        $this->db->like('barang_name', $word);
        $this->db->where(array('id_kategori' => $id));
        
        $query = $this->db->get($this->table_name);
        
        return $query->result();
    }
}