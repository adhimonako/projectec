<?php
class Order_Model extends CI_Model {

    public function __construct() {
		parent::__construct();

        ## declate table name here
        $this->table_name = 'ms_order' ;
    }

    function inputData() {
        $a_input = array();

        foreach ($_POST as $key => $value) {
            $a_input[$key] = strtolower($value);
        }

        $a_input['status'] = 0 ;
        $a_input['tanggal_pesan'] = date('Y-m-d');

        
        unset($a_input['id_penjual']);

        $this->db->insert($this->table_name, $a_input);
    }

    function editData($id) {
        ## unset supaya id tidak terambil
        unset($_POST['id']);

        foreach ($_POST as $key => $value) {
            $a_input[$key] = strtolower($value);
        }

        if (!empty($_FILES['bukti_bayar'])) {
            $a_input['bukti_bayar'] = $_FILES['bukti_bayar']['name'];
            $a_input['status'] = '2';
        }

        $this->db->where('id', $id);
        $this->db->update($this->table_name, $a_input);
    }

	public function deleteData($id) {
		$this->db->where('id', $id);
        $this->db->delete($this->table_name);

        return $id;
	}

    function getAllData() {
        $query = $this->db->get($this->table_name);

        return $query->result();
	}

    function getAllDataByID($id) {
        $this->db->where(array('ms_order.id' => $id));
        $this->db->select('u.* , b.*, ms_order.status,ms_order.tanggal_pesan,ms_order.id as id_order , ,ms_order.bukti_bayar');
        $this->db->join('ms_barang b', 'b.id = ms_order.id_barang', 'left');
        $this->db->join('ms_user u', 'u.id = ms_order.id_pembeli', 'left');

        $query = $this->db->get($this->table_name);
        
        return $query->result();
    }

    function getAllDataperUser($id) {
        $this->db->select('u.* , b.*, ms_order.status,ms_order.tanggal_pesan,ms_order.id as id_order,ms_order.lamaacara,ms_order.tanggalacara,ms_order.jammulai,ms_order.jamselesai');
        $this->db->where(array('b.id_penjual' => $id,'ms_order.statuspaket' => '0'));
        $this->db->join('ms_barang b', 'b.id = ms_order.id_barang', 'left');
        $this->db->join('ms_user u', 'u.id = ms_order.id_pembeli', 'left');

        $query = $this->db->get($this->table_name);

        return $query->result();
    }

    function getOrder($id) {
        $this->db->select('count(statuspaket) as pesanan');
        $this->db->where(array('b.id_penjual' => $id,'ms_order.statuspaket' => '0','ms_order.status' => '0'));
        $this->db->join('ms_barang b', 'b.id = ms_order.id_barang', 'left');
        

        $query = $this->db->get($this->table_name);

        return $query->row();
    }

    function getOrderPaket($id) {
        $this->db->select('count(ms_order_paket.id_paket) as pesanan');
        $this->db->where(array('b.id_penjual' => $id,'ms_order_paket.statuspaket' => '1','ms_order_paket.status' => '0'));
        $this->db->join('ms_paket b', 'b.id = ms_order_paket.id_paket', 'left');
        

        $query = $this->db->get('ms_order_paket');

        return $query->row();
    }


    function getOrderPembeli($id) {
        $this->db->select('b.*, ms_order.status,ms_order.tanggal_pesan,ms_order.id as id_order, ms_order.*,p.user_banknumber, p.nama_bank,p.user_phone');
        $this->db->where(array('ms_order.id_pembeli' => $id));
        $this->db->join('ms_barang b', 'b.id = ms_order.id_barang', 'left');
        $this->db->join('ms_user u', 'u.id = ms_order.id_pembeli', 'left');
        $this->db->join('ms_user p', 'p.id = b.id_penjual', 'left');

        $query = $this->db->get($this->table_name);

        return $query->result();
    }
}