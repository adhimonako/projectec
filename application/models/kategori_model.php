<?php
class kategori_Model extends CI_Model {

    public function __construct() {
		parent::__construct();

        ## declate table name here
        $this->table_name = 'ms_kategori' ;
    }

    function inputData() {
        $a_input = array();
       
        foreach ($_POST as $key => $value) {
            $a_input[$key] = strtolower($value);
        }

        $this->db->insert($this->table_name, $a_input);
    }

    function editData($id) {
        ## unset supaya id tidak terambil
        unset($_POST['id']);

        foreach ($_POST as $key => $value) {
            $a_input[$key] = strtolower($value);
        }

        $this->db->where('id', $id);
        $this->db->update($this->table_name, $a_input);
    }

	public function deleteData($id) {
		$this->db->where('id', $id);
        $this->db->delete($this->table_name);

        return $id;
	}

    function getAllData() {
        $query = $this->db->get($this->table_name);

        return $query->result();
	}

    function getAllDataByID($id) {
        $this->db->where(array('id' => $id));
        
        $query = $this->db->get($this->table_name);
        
        return $query->result();
    }

    function getAllDatabysearch($word) {
        
        $where = "barang_name like '%$word%' OR barang_region like '%$word%'";
        $this->db->where($where);
        $query = $this->db->get($this->table_name);

        return $query->result();
    }
}