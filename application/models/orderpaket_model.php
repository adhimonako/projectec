<?php
class Orderpaket_Model extends CI_Model {

    public function __construct() {
		parent::__construct();

        ## declate table name here
        $this->table_name = 'ms_order_paket' ;
    }

    function inputData() {
        $a_input = array();

        foreach ($_POST as $key => $value) {
            $a_input[$key] = strtolower($value);
        }

        $a_input['status'] = 0 ;
        $a_input['tanggal_pesan'] = date('Y-m-d');

        $this->db->insert($this->table_name, $a_input);
    }

    function editData($id) {
        ## unset supaya id tidak terambil
        unset($_POST['id']);

        foreach ($_POST as $key => $value) {
            $a_input[$key] = strtolower($value);
        }

        if (!empty($_FILES['bukti_bayar_paket'])) {
            $a_input['bukti_bayar'] = $_FILES['bukti_bayar_paket']['name'];
            $a_input['status'] = '2';
        }

        $this->db->where('id', $id);
        $this->db->update($this->table_name, $a_input);
    }

	public function deleteData($id) {
		$this->db->where('id', $id);
        $this->db->delete($this->table_name);

        return $id;
	}

    function getAllData() {
        $query = $this->db->get($this->table_name);

        return $query->result();
	}

    function getAllDataByID($id) {
        $this->db->where(array('ms_order_paket.id' => $id));
        
        $this->db->select('u.* , b.*,
             ms_order_paket.status,
             ms_order_paket.tanggal_pesan,
             ms_order_paket.id as id_order,
             ms_order_paket.bukti_bayar');
        
        $this->db->join('ms_paket b', 'b.id = ms_order_paket.id_paket', 'left');
        $this->db->join('ms_user u', 'u.id = ms_order_paket.id_pembeli', 'left');

        $query = $this->db->get($this->table_name);
        
        return $query->result();
    }

    function getAllDataperUser($id) {
         $this->db->select('u.* , b.*,ms_order_paket.id as id_order,ms_order_paket.*');

        $this->db->where(array('b.id_penjual' => $id,'ms_order_paket.statuspaket' => '1'));

        $this->db->join('ms_paket b', 'b.id = ms_order_paket.id_paket', 'left');
        $this->db->join('ms_user u', 'u.id = ms_order_paket.id_pembeli', 'left');

        $query = $this->db->get($this->table_name);

        return $query->result();
    }

    function getOrderPembeli($id) {
        $this->db->select('u.* , b.*,ms_order_paket.id as id_order,ms_order_paket.*,p.user_banknumber, p.nama_bank,p.user_phone');

        $this->db->where(array('ms_order_paket.id_pembeli' => $id));
        $this->db->join('ms_paket b', 'b.id = ms_order_paket.id_paket', 'left');
        $this->db->join('ms_user u', 'u.id = ms_order_paket.id_pembeli', 'left');
        $this->db->join('ms_user p', 'p.id = b.id_penjual', 'left');

        $query = $this->db->get($this->table_name);

        return $query->result();
    }
}