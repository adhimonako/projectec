<?php
class Paket_Model extends CI_Model {

    public function __construct() {
		parent::__construct();

        ## declate table name here
        $this->table_name = 'ms_paket' ;
    }

    function inputData() {
        $a_input = array();
       
        foreach ($_POST as $key => $value) {
            $a_input[$key] = $value;
        }

        $a_input['id_barang'] = implode(',',$_POST['id_barang']);

        $this->db->insert($this->table_name, $a_input);
    }

    function editData($id) {
        ## unset supaya id tidak terambil
        unset($_POST['id']);

        foreach ($_POST as $key => $value) {
            $a_input[$key] = strtolower($value);
        }

        $a_input['id_barang'] = implode(',',$_POST['id_barang']);
        
        if (!empty($_FILES['photo_paket'])) {
            $a_input['photo_paket'] = $_FILES['photo_paket']['name'];
        }

        if (empty($a_input['photo_paket'])) {
            unset($a_input['photo_paket']);
        }

        //

        if (!empty($_FILES['photo_paket1'])) {
            $a_input['photo_paket1'] = $_FILES['photo_paket1']['name'];
        }

        if (empty($a_input['photo_paket1'])) {
            unset($a_input['photo_paket1']);
        }

        //
        if (!empty($_FILES['photo_paket2'])) {
            $a_input['photo_paket2'] = $_FILES['photo_paket2']['name'];
        }

        if (empty($a_input['photo_paket2'])) {
            unset($a_input['photo_paket2']);
        }

        //

        if (!empty($_FILES['photo_paket3'])) {
            $a_input['photo_paket3'] = $_FILES['photo_paket3']['name'];
        }

        if (empty($a_input['photo_paket3'])) {
            unset($a_input['photo_paket3']);
        }

        //

        if (!empty($_FILES['photo_paket4'])) {
            $a_input['photo_paket4'] = $_FILES['photo_paket4']['name'];
        }

        if (empty($a_input['photo_paket4'])) {
            unset($a_input['photo_paket4']);
        }

        $this->db->where('id', $id);
        $this->db->update($this->table_name, $a_input);
    }

	public function deleteData($id) {
		$this->db->where('id', $id);
        $this->db->delete($this->table_name);

        return $id;
	}

    function getAllData() {
        $query = $this->db->get($this->table_name);

        return $query->result();
	}

    function getAllDataByID($id) {
        $this->db->where(array('id' => $id));
        
        $query = $this->db->get($this->table_name);
        
        return $query->result();
    }

    function getAllDataperUser($id) {
        $this->db->where(array('id_penjual' => $id));
        $query = $this->db->get($this->table_name);

        return $query->result();
    }

    function getAllDataByIDKategori($id) {
        $this->db->where(array('id_kategori' => $id));
        
        $query = $this->db->get($this->table_name);
        
        return $query->result();
    }

    function getAllDatabysearch($word) {
        
        $where = "nama_paket like '%$word%' OR harga_paket like '%$word%' OR tradisi_paket like '%$word%' ";
        $this->db->where($where);
        $query = $this->db->get($this->table_name);

        return $query->result();
    }
}