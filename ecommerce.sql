-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 15, 2018 at 11:10 AM
-- Server version: 10.1.25-MariaDB
-- PHP Version: 5.6.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ecommerce`
--

-- --------------------------------------------------------

--
-- Table structure for table `ms_barang`
--

CREATE TABLE `ms_barang` (
  `id` int(11) NOT NULL,
  `id_penjual` varchar(45) DEFAULT NULL,
  `id_kategori` varchar(45) DEFAULT NULL,
  `barang_name` varchar(45) DEFAULT NULL,
  `barang_price` varchar(45) DEFAULT NULL,
  `barang_region` varchar(45) DEFAULT NULL,
  `barang_desc` text,
  `photo_barang` varchar(100) NOT NULL,
  `photo_barang1` text NOT NULL,
  `photo_barang2` text NOT NULL,
  `photo_barang3` text NOT NULL,
  `photo_barang4` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ms_barang`
--

INSERT INTO `ms_barang` (`id`, `id_penjual`, `id_kategori`, `barang_name`, `barang_price`, `barang_region`, `barang_desc`, `photo_barang`, `photo_barang1`, `photo_barang2`, `photo_barang3`, `photo_barang4`) VALUES
(3, '4', '1', '2', '2', '2', '2', '', '', '', '', ''),
(4, '4', '1', 'x', '100000000', 'x', 'x', '', '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `ms_kategori`
--

CREATE TABLE `ms_kategori` (
  `id` int(11) NOT NULL,
  `kategori_name` varchar(45) DEFAULT NULL,
  `kategori_desc` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ms_kategori`
--

INSERT INTO `ms_kategori` (`id`, `kategori_name`, `kategori_desc`) VALUES
(1, 'photografer', 'photografer, hunting, preweding');

-- --------------------------------------------------------

--
-- Table structure for table `ms_order`
--

CREATE TABLE `ms_order` (
  `id` int(100) NOT NULL,
  `id_barang` int(100) NOT NULL,
  `id_pembeli` int(100) NOT NULL,
  `status` text NOT NULL,
  `bukti_bayar` text NOT NULL,
  `tanggal_pesan` date NOT NULL,
  `tanggalacara` date NOT NULL,
  `lamaacara` int(100) NOT NULL,
  `statuspaket` int(11) NOT NULL DEFAULT '0',
  `jammulai` time NOT NULL,
  `jamselesai` time NOT NULL,
  `alasan` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ms_order`
--

INSERT INTO `ms_order` (`id`, `id_barang`, `id_pembeli`, `status`, `bukti_bayar`, `tanggal_pesan`, `tanggalacara`, `lamaacara`, `statuspaket`, `jammulai`, `jamselesai`, `alasan`) VALUES
(1, 4, 5, '3', '', '2018-01-23', '2018-01-23', 2, 0, '00:00:00', '12:00:00', 'cdnfnnl  rgjwrig');

-- --------------------------------------------------------

--
-- Table structure for table `ms_order_paket`
--

CREATE TABLE `ms_order_paket` (
  `id` int(100) NOT NULL,
  `id_paket` int(100) NOT NULL,
  `id_pembeli` int(100) NOT NULL,
  `status` varchar(100) NOT NULL,
  `bukti_bayar` text NOT NULL,
  `tanggal_pesan` text NOT NULL,
  `tanggalacara` date NOT NULL,
  `lamaacara` int(200) NOT NULL,
  `statuspaket` int(100) NOT NULL DEFAULT '1',
  `jammulai` time NOT NULL,
  `jamselesai` time NOT NULL,
  `alasan` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ms_order_paket`
--

INSERT INTO `ms_order_paket` (`id`, `id_paket`, `id_pembeli`, `status`, `bukti_bayar`, `tanggal_pesan`, `tanggalacara`, `lamaacara`, `statuspaket`, `jammulai`, `jamselesai`, `alasan`) VALUES
(1, 1, 5, '3', '', '2018-01-23', '2018-01-23', 3, 1, '11:11:00', '23:11:00', 'nacnadcjk cnen iofjqrio');

-- --------------------------------------------------------

--
-- Table structure for table `ms_paket`
--

CREATE TABLE `ms_paket` (
  `id` int(100) NOT NULL,
  `nama_paket` text NOT NULL,
  `harga_paket` text NOT NULL,
  `deskripsi_paket` text NOT NULL,
  `tradisi_paket` text NOT NULL,
  `id_barang` varchar(100) NOT NULL,
  `id_penjual` int(100) NOT NULL,
  `photo_paket` text NOT NULL,
  `photo_paket1` text NOT NULL,
  `photo_paket2` text NOT NULL,
  `photo_paket3` text NOT NULL,
  `photo_paket4` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ms_paket`
--

INSERT INTO `ms_paket` (`id`, `nama_paket`, `harga_paket`, `deskripsi_paket`, `tradisi_paket`, `id_barang`, `id_penjual`, `photo_paket`, `photo_paket1`, `photo_paket2`, `photo_paket3`, `photo_paket4`) VALUES
(1, 'djf', '1111', 'fjjwrfrrrrrrrrrrrrrrrrrrrrrrrrrrrwdwwe', 'frwjfnrfjwrnfwefwejn', '3,4', 4, '', '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `ms_role`
--

CREATE TABLE `ms_role` (
  `id` int(11) NOT NULL,
  `role_name` varchar(45) DEFAULT NULL,
  `role_desc` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ms_role`
--

INSERT INTO `ms_role` (`id`, `role_name`, `role_desc`) VALUES
(1, 'admin', 'ujhvefjkvke'),
(2, 'penjual', 'pembeli'),
(3, 'pembeli', 'penjual\r\n'),
(6, 'penjual', 'penjual'),
(7, 'admin1', 'admin1');

-- --------------------------------------------------------

--
-- Table structure for table `ms_user`
--

CREATE TABLE `ms_user` (
  `id` int(11) NOT NULL,
  `user_name` varchar(45) DEFAULT NULL,
  `user_phone` varchar(45) DEFAULT NULL,
  `user_address` varchar(45) DEFAULT NULL,
  `user_mail` varchar(45) DEFAULT NULL,
  `user_password` varchar(100) DEFAULT NULL,
  `user_roleid` int(100) NOT NULL,
  `user_bussiness` varchar(100) NOT NULL,
  `user_banknumber` varchar(100) NOT NULL,
  `user_bussiness_photo` text NOT NULL,
  `user_photo` text NOT NULL,
  `user_status_langganan` int(100) NOT NULL,
  `user_jenis_langganan` varchar(100) NOT NULL,
  `user_photo_langganan` varchar(100) NOT NULL,
  `nama_bank` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ms_user`
--

INSERT INTO `ms_user` (`id`, `user_name`, `user_phone`, `user_address`, `user_mail`, `user_password`, `user_roleid`, `user_bussiness`, `user_banknumber`, `user_bussiness_photo`, `user_photo`, `user_status_langganan`, `user_jenis_langganan`, `user_photo_langganan`, `nama_bank`) VALUES
(1, 'admin', '089899', 'admin', 'aa@mail.com', 'admin', 1, '1000', '988776765435789', '', '', 0, '', '', ''),
(4, 'coky', '12345', 'sampang lima', 'coky@yahoo.comm', 'coky', 2, 'weddingxxxx', '100000', '', '', 1, '1 bulan - rp 50.000', 'xxxxx_yxxxxxxx_zxxxxx.PNG', 'bin coky'),
(5, 'vicky', '09786767', 'sampang', 'vicky@yahoo.com', 'vicky', 3, '', '', '', 'Tulips.jpg', 0, '', '', ''),
(6, 'b', '1', 'b', 'b@mail.com', 'b', 2, 'b', '1', '', '', 1, '1 bulan - rp 50.000', 'x.PNG', 'b');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `ms_barang`
--
ALTER TABLE `ms_barang`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ms_kategori`
--
ALTER TABLE `ms_kategori`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ms_order`
--
ALTER TABLE `ms_order`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ms_order_paket`
--
ALTER TABLE `ms_order_paket`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ms_paket`
--
ALTER TABLE `ms_paket`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ms_role`
--
ALTER TABLE `ms_role`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ms_user`
--
ALTER TABLE `ms_user`
  ADD PRIMARY KEY (`id`),
  ADD KEY `role` (`user_roleid`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `ms_barang`
--
ALTER TABLE `ms_barang`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `ms_kategori`
--
ALTER TABLE `ms_kategori`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `ms_order`
--
ALTER TABLE `ms_order`
  MODIFY `id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `ms_order_paket`
--
ALTER TABLE `ms_order_paket`
  MODIFY `id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `ms_paket`
--
ALTER TABLE `ms_paket`
  MODIFY `id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `ms_role`
--
ALTER TABLE `ms_role`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `ms_user`
--
ALTER TABLE `ms_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
